def gentests1(n):
    A = ["x0 = 0"]
    for i in range(1, n+1):
        A.append("x"+str(i)+" = x"+str(i-1)+" + 1")
    return A

def gentests2(n):
    A = ["x0 = 0", "x1 = 1"]
    for i in range(2, n+1):
        A.append("x"+str(i)+" = x"+str(i-1)+" - x"+str(i-2))
    return A

def convert(L):
    acc = []
    for (k, v) in enumerate(L):
        if k == 0:
            acc.append("do "+v)
        else:
            acc.append("and "+v)
    return acc

def nodify(L):
    acc = []
    n = len(L)
    acc.append("let node main() returns (x"+str(n-1)+")")
    s = "local "
    for i in range(n-2):
        s += "x"+str(i)+","
    s += "x"+str(n-2)
    acc.append(s)
    acc += L
    acc.append("done")
    return acc

def write(L, name):
    with open("gentests/"+name+".zls", "w") as f:
        for v in L:
            f.write(v+"\n")
        f.close()

def main():
    n = 200
    L0 = gentests1(n)
    #L0 = gentests2(n)
    L1 = convert(L0)
    L2 = convert(L0[::-1])
    N1 = nodify(L1)
    N2 = nodify(L2)
    write(N1, "n"+str(n))
    write(N2, "r"+str(n))

main()

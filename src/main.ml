(*
https://gist.github.com/23Skidoo/1658338
*)
open Ast
open Location
open Bench

let swap arr i j =
  let temp = arr.(i) in
  arr.(i) <- arr.(j);
  arr.(j) <- temp

let fisher_yates_shuffle arr =
  let l = Array.length arr in
  for i = (l-1) downto 1 do
    let r = Random.int (i+1) in
    swap arr i r;
  done;
  ()

let random_permutation n =
  let a = Array.init n (fun k -> k+1) in
  fisher_yates_shuffle a;
  Array.to_list a

(* k permutations de longueur n *)
let random_permutations k n =
  let ps = Array.make k [] in
  let p = Array.init n (fun i -> i+1) in
  for i = 1 to k do
    fisher_yates_shuffle p;
    ps.(i-1) <- (Array.to_list p);
  done;
  Array.to_list ps


let rd_const n =
  let i = Random.int 3 in
  match i with
  | 0 -> let r = Random.int n in Eint r
  | 1 -> let r = Random.int 2 in if r = 1 then Ebool true else Ebool false
  | 2 -> let r = Random.int n in Efloat (float_of_int r)

let localize x = { desc = x; loc = Loc (0, 0) }
let localize2 x = { e_desc = x; e_loc = Loc (0, 0) }
let localize3 x = { eq_desc = x; eq_write = S.empty; eq_loc = Loc (0, 0) }

let x k = "x" ^ (string_of_int k)
let gen k = Ident.fresh (x k)
module M = Map.Make(Int)
let genx n = List.fold_left (fun m k -> M.add k (gen k) m) M.empty (List.init (n+1) (fun k -> k))
let genexp x k =
  if k = 0 then (Econst (Eint 0))
  else Elocal (M.find k x)
let geneq x k =
  let eqn_dp = localize [M.find k x] in
  let eqn_de = genexp x (k-1) in
  (*let eqn_de = localize2 eqn_de in*)
  let eqn_de = localize2 (Eapp (Name "+", List.map localize2 [eqn_de ; Econst (Eint 1)])) in
  let eqn_d = EQeq (eqn_dp, eqn_de) in
  let eqn_w = S.empty in
  let eqn_l = Loc (0, 0) in
  { eq_desc = eqn_d; eq_write = eqn_w; eq_loc = eqn_l }
let geneqand x n perm =
  let eqns = List.fold_left (fun l k -> (geneq x k)::l) [] perm in
  List.rev eqns

let genres x k =
  { var_name = M.find k x;
    var_default = Ewith_nothing;
    var_loc = Loc (0, 0) }
let genfunexp x n perm =
   let lvd = List.map (genres x) (List.init (n-1) (fun k -> k+1)) in
   let eqand = EQand (geneqand x n perm) in
   let eqlocal = EQlocal (lvd, localize3 eqand) in
   { f_kind = Enode;
     f_atomic = false;
     f_args = [];
     f_res = [genres x n];
     f_body = localize3 eqlocal;
     f_loc = Loc (0, 0) }
let genprogram n perm =
  let x = genx n in
  let fnexp = genfunexp x n perm in
  [localize (Eletfundecl ("main", fnexp))]

let main n =
  genprogram n
  

(*
(*
https://codereview.stackexchange.com/questions/125173/generating-permutations-in-ocaml
*)
let rec permutations result other = function
  | [] -> [result]
  | hd :: tl ->
     let r = permutations (hd :: result) [] (other @ tl) in
     if tl <> [] then
       r @ permutations result (hd :: other) tl
     else
       r
in
let pn n = permutations [] [] (List.init n (fun k -> k+1))
in
let rec tamis_aux (cur, ok, not_ok) = function
| [] -> cur, ok, not_ok
| a::b -> if a = cur + 1
          then tamis_aux (cur+1, ok@[a], not_ok) b
          else tamis_aux (cur, ok, not_ok@[a]) b
in
let tamis l cur = tamis_aux (cur, [], []) l
in
let scan l =
  let rec scan_aux (cur, ok, not_ok, oks) = function
  | [] -> cur, ok, not_ok, oks
  | l -> let cur2, ok2, not_ok2 = tamis_aux (cur, ok, not_ok) l in
         scan_aux (cur2, [], [], ok2::oks) not_ok2
  in
  let cur, ok, not_ok, oks = scan_aux (0, [], [], []) l in
  let oks = List.filter (fun l -> l <> []) oks in
  if ok <> []
  then (cur, ok::oks, not_ok)
  else (cur, oks, not_ok)
in
let scan2 l =
  let rec scan2_aux acc = function
  | [] -> acc
  | [_] -> acc
  | a::b::l -> if a < b
               then scan2_aux acc (b::l)
               else scan2_aux (acc+1) (b::l)
  in
  let acc = scan2_aux 1 l
  in acc
in
(* let print l =
  print_endline "<--";
  List.iter (fun v -> print_endline (string_of_int v)) l;
  print_endline "-->"
in*)
let print2 l =
  print_endline (List.fold_left (fun a b -> a ^ (string_of_int b)) "" l);
in
let factorielle n =
  let rec factorielle_aux acc = function
  | 0 -> acc
  | 1 -> acc
  | n -> factorielle_aux (n*acc) (n-1)
  in
  factorielle_aux 1 n
in
(* counting sort for integers within {1, n} *)
let counting_sort n l =
  print_endline ("--- " ^ (string_of_int n));
  let a = Array.make n 0 in
  (* List.iter (fun v -> print_endline (string_of_int v)) l;*)
  List.iter (fun v -> a.(v-1) <- a.(v-1) + 1) l;
  Array.iteri (fun i v ->
    print_endline (
        (string_of_int (i+1))
      ^ " : "
      ^ (string_of_int v)
    )
  ) a;
  let total = Array.fold_left (+) 0 a in
  let i = ref 0 in
  let totalp = Array.fold_left
    (fun a b -> incr i; a + b * !i)
    0
    a
  in
  print_endline ("total : " ^ (string_of_int total));
  print_endline ("total pondéré : " ^ (string_of_int totalp));
  ()
in

let main3 n =
  let l = pn n in
  let ll = List.map
    (fun l -> (let _, b, _ = scan l in List.length b, scan2 l, l)) l in
  let lll = List.filter (fun (a, b, c) -> a <> b) ll in
  print_endline (string_of_int (List.length ll));
  print_endline (string_of_int (List.length lll));
  if n <= 5
  then List.iter print2 (List.map (fun (a, b, c) -> c) lll);
  ()
in

let main2 n =
  let l = pn n in
  let ll = List.map (fun l -> scan2 l) l in
  let s = List.fold_left (+) 0 ll in
  print_endline (
    (string_of_int n)
    ^ " : " ^
    (string_of_int s)
  );
  ll
in
let main n =
(*        List.iter (fun k -> print_endline (string_of_int (factorielle k))) [0;1;2;3;4;5;6;7];*)
  let l = pn n in
  let len = List.length l in
  (*print_endline ("Nombre de permutations : "^(string_of_int len)^".");*)
  let ll = List.map (fun l ->
    (* print2 l; *)
    let cur, oks, not_ok = scan l in
    let loks = List.length oks in
    (* let lnot_ok = List.length not_ok in
    print_endline
      ("  "
      ^ (string_of_int cur)         
      ^ " / "
      ^ (string_of_int loks)
      ^ " / "
      ^ (string_of_int lnot_ok)); *)
    loks

  ) l
  in
  ll;
  (* print2 ll; *)
  let s = List.fold_left (+) 0 ll in
  print_endline (
    (string_of_int n)
    ^ " : " ^
    (* Total prévu *)
    (string_of_int ((factorielle (n+1))/2))
    ^ " / " ^
    (* Total effectif *)
    (string_of_int s)
  );
  ll
  (* let _, oks, _ = scan [2;3;6;8;10;1;4;9;7;5] in
  print_endline (string_of_int (List.length oks)) *)

  (*
  print_endline "Tamis";
  let t = [3;2;1] in
  let cur, t1, t2 = tamis t 0 in
  print t1;
  print t2;
  let cur2, t3, t4 = tamis t2 cur in
  print t3;
  print t4;
  print_endline (string_of_int cur);
  
  let cur2, oks, not_ok = scan t in
  let l3, l4 = List.length oks, List.length not_ok in
  print_endline "Scan";
  print_endline ((string_of_int l3)^" "^(string_of_int l4));
  List.iter (fun l -> print l) oks;
  *)
in
let ll1 = List.map main (List.init 8 (fun k -> k+1)) in
let ll2 = List.map main2 (List.init 8 (fun k -> k+1)) in
(**)
List.iteri (fun n v -> counting_sort (n+1) v) ll1;
List.iteri (fun n v -> counting_sort (n+1) v) ll2;
(**)
(*
let n = 2 in
counting_sort n (List.nth ll (n-1));
*)
let l = [1;7;2;4;6;8;3;5] in
let cur, oks, not_ok = scan l in
let t = scan2 l in
let l1 = List.length oks in
print_endline ((string_of_int l1) ^ " : " ^ (string_of_int t));
let ll2 = List.map main3 (List.init 9 (fun k -> k+1)) in
()
(* faire l'inventaire des permutations pour lesquelles le résultat est différent, à n fixé *)
*)


(* *********************************************************************)
(*                                                                     *)
(*                        The ZRun Interpreter                         *)
(*                                                                     *)
(*                             Marc Pouzet                             *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique. All rights reserved. This file is distributed under   *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(* A Denotational Semantics for Zelus *)
(* This is a companion file of working notes on the co-iterative *)
(* semantics presented at the SYNCHRON workshop, December 2019, *)
(* the class on "Advanced Functional Programming" given at Bamberg
   Univ. in June-July 2019 and the Master MPRI - M2, Fall 2019 *)
open Misc
open Monad
open Opt                                                            
open Ident
open Ast
open Value
open Initial
open Debug

open Cowrite

open Common
   
let find_value_opt x env =
  let* { cur } = Env.find_opt x env in
  return cur

let find_last_opt x env =
  let* { default } = Env.find_opt x env in
  match default with
  | Last(v) -> return v
  | _ -> None
           
let find_gnode_opt x env =
  let* v = Genv.find_opt x env in
  match v with
  | Gfun(f) -> return f
  | _ -> None

let find_gvalue_opt x env =
  let* v = Genv.find_opt x env in
  match v with
  | Gvalue(v) -> return v
  | _ -> None

let names env = Env.fold (fun n _ acc -> S.add n acc) env S.empty

(* value of an immediate constant *)
let value v =
  match v with
  | Eint(v) -> Vint(v)
  | Ebool(b) -> Vbool(b)
  | Evoid -> Vvoid
  | Efloat(f) -> Vfloat(f)
  | Echar(c) -> Vchar(c)
  | Estring(s) -> Vstring(s)


(* evaluation functions *)

(* the bottom environment *)
let bot_env eq_write =
  S.fold (fun x acc -> Env.add x { cur = Vbot; default = Val } acc)
    eq_write Env.empty

(* the nil environment *)
let nil_env eq_write =
  S.fold (fun x acc -> Env.add x { cur = Vnil; default = Val } acc)
    eq_write Env.empty

let size { eq_write } = S.cardinal eq_write

(* a bot/nil value lifted to lists *)
let bot_list l = List.map (fun _ -> Vbot) l
let nil_list l = List.map (fun _ -> Vnil) l

let eptf =
  if !set_verbose
  then Format.eprintf
  else (fun x -> ())

(* merge two environments provided they do not overlap *)
let merge env1 env2 =
  let s = Env.to_seq env1 in
  seqfold
    (fun acc (x, entry) ->
      if Env.mem x env2 then None
      else return (Env.add x entry acc))
    env2 s


(* given an environment [env], a local environment [env_handler] *)
(* an a set of written variables [write] *)
(* complete [env_handler] with entries in [env] for variables that *)
(* appear in [write] *)
(* this is used for giving the semantics of a control-structure, e.g.,: *)
(* [if e then do x = ... and y = ... else do x = ... done]. When [e = false] *)
(* the value of [y] is the default one given at the definition of [y] *)
(* for the moment, we treat the absence of a default value as a type error *)
let by env env_handler write =
  S.fold
    (fun x acc ->
      (* if [x in env] but not [x in env_handler] *)
      (* then either [x = last x] or [x = default(x)] depending *)
      (* on what is declared for [x]. *)
      let* { default } as entry = Env.find_opt x env in
      if Env.mem x env_handler then acc
      else
        match default with
        | Val -> None
        | Last(v) | Default(v) ->
           let* acc = acc in
           return (Env.add x { entry with cur = v } acc))
    write (return env_handler) 
       
(* complete [env_handler] with inputs from [write] *)
(* pre-condition: [Dom(env_handler) subseteq write] *)
let complete env env_handler write =
  S.fold
    (fun x acc ->
      match Env.find_opt x env_handler with
      | Some(entry) -> Env.add x entry acc
      | None ->
         match Env.find_opt x env with
         | Some(entry) -> Env.add x entry acc
         | None -> acc (* this case should not arrive *))
    write Env.empty
  
(* given [env] and [env_handler = [x1 \ { cur1 },..., xn \ { curn }] *)
(* returns [x1 \ { cu1; default x env },..., xn \ { curn; default x env }] *)
(* see src/common.ml : complete_with_default env env_handler *)

 (* equality of values in the fixpoint iteration. Because of monotonicity *)
(* only compare bot/nil with non bot/nil values *)
let equal_values v1 v2 =
  match v1, v2 with
  | (Vbot, Vbot) | (Vnil, Vnil) | (Value _, Value _) -> true
  | _ -> false

let _valuation = ref (fun x env -> Env.find_opt x env)
(* Dynamic check of causality. A set of equations is causal when all defined *)
(* variables are non bottom, provided free variables are non bottom *)
(* this a very strong constraint. In particular, it rejects the situation *)
(* of a variable that is bottom but not used. *)
(* causal(env)(env_out)(names) =
 *-               /\ (forall x in Dom(env), env(x) <> bot)
 *-               /\ (env_out, _ = fixpoint_eq genv sem eq n s_eq bot)
 *-               => (forall x in names /\ Dom(env_out), env_out(x) <> bot) *)
let causal env env_out names =
  let bot v = match v with | Vbot -> true | _ -> false in
  let bot_name n =
    let r = find_value_opt n env_out in
    match r with | None -> false | Some(v) -> bot v in
  let bot_names =
    if Env.for_all (fun _ { cur } -> not (bot cur)) env
    then S.filter bot_name names else S.empty in
  let pnames ff names = S.iter (Ident.fprint_t ff) names in
  if not !set_nocausality then
    if S.is_empty bot_names then ()
    else
      begin
        Format.eprintf "The following variables are not causal:\n\
                        %a@." pnames bot_names;
        raise Stdlib.Exit
      end

(* bounded fixpoint combinator *)
(* computes a pre fixpoint f^n(bot) <= fix(f) *)
let fixpoint n equal f s bot =
  let rec fixpoint n v =
    if n <= 0 then (* this case should not happen *)
      return (0, v, s)
    else
      (* compute a fixpoint for the value [v] keeping the current state *)
      let* v', s' = f s v in
      (*
      print_ienv "A:" v;
      print_ienv "B:" v';
      *)
      if equal v v' then return (n, v, s') else fixpoint (n-1) v' in      
  (* computes the next state *)
  fixpoint n bot
  
let equal_env env1 env2 =
  Env.equal
    (fun { cur = cur1} { cur = cur2 } -> equal_values cur1 cur2) env1 env2


(* let _f = ref (fun () -> Env.empty) *)

(* bounded fixpoint for a set of equations *)
let fixpoint_eq genv env sem eq n s_eq bot =
  print_number "Max number of iterations:" n;
  print_ienv "Fixpoint. Initial env is:" bot;
  (* printm (cowrite_eq eq s_eq); *)
 
  let sem2 s_eq env_in =
    let env = Env.append env_in env in
    let* env_out, s_eq = sem genv env eq s_eq in
    let env_out = complete_with_default env env_out in
    return (env_out, s_eq)
  in
  let* m, env_out (*, s_eq*) = match !eval_method with
  | 2 ->
    (* Format.eprintf "old fixpoint\n";*)
    let* a, b, c = fixpoint n equal_env sem2 s_eq bot in
    begin match eq.eq_desc with
    | EQand _ -> fixpoint_count := !fixpoint_count + (n-a)
    | _ -> ()
    end;
    return (a, b)
  | 4 ->
    (* Format.eprintf "new fixpoint, P4, exceptions\n"; *)
    let write = S.elements eq.eq_write in
    (*
    List.iter (fun id -> Format.eprintf "%s; " (string_of id)) write;
    Format.eprintf "\n";
    *)
    let valuation = Lfp4.Make.globeval write sem2 s_eq in
    let* env_out2 = Opt.fold
      (fun env id ->
         try
           let v = valuation env id in
           return (Env.add id v env)
         with
         | Not_found -> return env
      )
      env write
    in
    return (0, env_out2)
  | 5 ->
    let write = S.elements eq.eq_write in
    let valuation, getenv = Lfp5.Make.globeval write sem2 s_eq in
    let renv = getenv () in
    renv := bot;
    let _old_valuation = !_valuation in
    eptf "<CHANGE>\n";
    _valuation := (fun x _ -> Some (valuation x));
    List.iter (fun id ->
      let _ = valuation id in
      ()
    ) write;
    _valuation := _old_valuation;
    eptf "</CHANGE>\n";
    let env_out = !(getenv ()) in
    print_ienv "OUT: "env_out;
    return (0, env_out)
  | 6 | 7 ->
    let write = S.elements eq.eq_write in
    let cowrite = cowrite_eq eq s_eq in
    let sem3 s_eq env_in id =
      if !set_verbose then Format.eprintf  "SEM3 : %s\n" (string_of id);
      let env = Env.append env_in env in
      (* List.iter (fun v -> Format.eprintf "%s, " (string_of v)) (List.map fst (Env.bindings cowrite));
      Format.eprintf "\n"; *)
      (* if !set_verbose then printm cowrite; *)
      let subeq, subs = if is_local eq then eq, s_eq else Env.find id cowrite in
      (* if !set_verbose then printm (Env.add id (subeq, subs) Env.empty);*)
      let* env_out, s_eq = sem genv env subeq subs in
      let env_out = complete_with_default env env_out in
      return (env_out, s_eq)
    in
    let valuation, getenv = if !eval_method = 6
      then Lfp6.Make.globeval write sem3 s_eq
      else Lfp7.Make.globeval write sem3 s_eq
    in
    if !set_verbose2 then
      Format.eprintf "Starting fixpoint computation...\n";
    let renv = getenv () in
    renv := bot;
    let _old_valuation = !_valuation in
    eptf "<CHANGE>\n";
    if false
    then _valuation := (fun x _ -> Some (valuation x))
    else _valuation := (fun x env -> let v = try valuation x
                                             with | Not_found -> Env.find x env
                                     in Some(v));
    (*       Some v
        with
         | Not_found ->
         
           if !set_verbose then Format.eprintf "-> %s : val.\n" (string_of x);
           let v =
                   (* try *)
             valuation x
           (* with
           | Not_found -> Format.eprintf "TROUVÉ : %s" (string_of x); Env.find x env *)
           in
           (* if !set_verbose then begin
           Format.eprintf "-> %s : val : " (string_of x);
           Pprinter.pp_value_ext v.cur;
           Format.eprintf "\n";
           end;*)
           Some v
    );*)

    List.iter (fun id ->
      (* Format.eprintf "#3 : %s\n" (string_of id);*)
      let _ = !_valuation id !renv in
      ()
    ) write;
    _valuation := _old_valuation;
    eptf "</CHANGE>\n";
    let env_out = !(getenv ()) in
    print_ienv "OUT: "env_out;
    return (0, env_out)
 
  in
  print_ienv "Fixpoint. Result env is:" env_out;
  if !eval_method = 2
    then print_number "Actual number of iterations:" (n - m);
  print_number "Max was:" n;
  print_ienv "End of fixpoint with env:" env_out;
  return (env_out, s_eq)
 
(* [sem genv env e = CoF f s] such that [iexp genv env e = s] *)
(* and [sexp genv env e = f] *)
(* initial state *)
let rec iexp genv env { e_desc; e_loc } =
  let r = match e_desc with
  (* Econstr0 Eglobal Elast *)
  | Econst _ | Elocal _ ->
     return Sempty
  (* | Econstr1(_, e_list) ->
     let* s_list = Opt.map (iexp genv env) e_list in
     return (Stuple(s_list)) *)
  | Eop(op, e_list) ->
     begin match op, e_list with
     | Efby, [{ e_desc = Econst(v) }; e] ->
        (* synchronous register initialized with a static value *)
        let* s = iexp genv env e  in
        return (Stuple [Sval(Value (value v)); s])
     | Efby, [e1; e2] ->
        let* s1 = iexp genv env e1 in
        let* s2 = iexp genv env e2 in
        return (Stuple [Sopt(None); s1; s2])
     | Eunarypre, [e] -> 
        (* un-initialized synchronous register *)
        let* s = iexp genv env e in
        return (Stuple [Sval(Vnil); s])
     | Eminusgreater, [e1; e2] ->
        let* s1 = iexp genv env e1 in
        let* s2 = iexp genv env e2 in
        return (Stuple [Sval(Value(Vbool(true))); s1; s2])
     | Eifthenelse, [e1; e2; e3] ->
          let* s1 = iexp genv env e1 in
          let* s2 = iexp genv env e2 in
          let* s3 = iexp genv env e3 in
          return (Stuple [s1; s2; s3])
     | _ -> None
     end
  | Etuple(e_list) -> 
     let* s_list = Opt.map (iexp genv env) e_list in
     return (Stuple(s_list))
  (* | Eget(_, e) ->
     let* s = iexp genv env e in
     return s *)
  | Eapp(f, e_list) ->
     let* s_list = Opt.map (iexp genv env) e_list in
     let* v = find_gnode_opt f genv in
     let s =
       match v with | CoFun _ -> Sempty | CoNode { init = s } -> s in
     return (Stuple(s :: s_list))
  | Elet(is_rec, eq, e) ->
     let* s_eq = ieq genv env eq in
     let* se = iexp genv env e in
     return (Stuple [s_eq; se]) in
  stop_at_location e_loc r 1
    
and ieq genv env { eq_desc; eq_loc } =
  let r = match eq_desc with
  | EQeq(_, e) -> iexp genv env e
  | EQand(eq_list) ->
     let* seq_list = Opt.map (ieq genv env) eq_list in
     return (Stuple seq_list)
  | EQlocal(v_list, eq) ->
     let* s_v_list = Opt.map (ivardec genv env) v_list in
     let* s_eq = ieq genv env eq in
     return (Stuple [Stuple s_v_list; s_eq])
  in
  stop_at_location eq_loc r 2

and ivardec genv env { var_default; var_loc } =
  let r = match var_default with
  | Ewith_init(e) ->
     let* s = iexp genv env e in return (Stuple [Sopt(None); s])
  | Ewith_default(e) -> iexp genv env e
  | Ewith_nothing -> return Sempty
  | Ewith_last -> return (Sval(Vnil)) in
  stop_at_location var_loc r 3

(* pattern matching *)
(* match the value [v] against [x1,...,xn] *)
let rec matching_pateq { desc } v =
  match desc, v with
  | [x], _ -> return (Env.singleton x { cur = v; default = Val })
  | x_list, Vbot -> matching_list Env.empty x_list (bot_list x_list)
  | x_list, Vnil -> matching_list Env.empty x_list (nil_list x_list)
  | x_list, Value(Vtuple(v_list)) -> matching_list Env.empty x_list v_list
  | _ -> Format.eprintf "FINI\n"; None

and matching_list env x_list v_list =
    match x_list, v_list with
    | [], [] ->
       return env
    | x :: x_list, v :: v_list ->
       matching_list (Env.add x { cur = v; default = Val } env) x_list v_list
    | _ -> Format.eprintf "FINI2\n"; None
         
(* match a value [ve] against a pattern [p] *)
let matching_pattern ve { desc } =
  match ve, desc with
  | Vconstr0(f), Econstr0pat(g) when Lident.compare f g = 0 -> Some(Env.empty)
  | _ -> None
       
(* [reset init step genv env body s r] resets [step genv env body] *)
(* when [r] is true *)
let reset init step genv env body s r =
  let*s = if r then init genv env body else return s in
  step genv env body s
  
(* step function *)
(* the value of an expression [e] in a global environment [genv] and local *)
(* environment [env] is a step function of type [state -> (value * state) option] *)
(* [None] is return in case of type error; [Some(v, s)] otherwise *)
let rec sexp genv env ({ e_desc = e_desc; e_loc } as exp) s =
  let r = match e_desc, s with   
  | Econst(v), s ->
     return (Value (value v), s)
  (* | Econstr0(f), s ->
     return (Value (Vconstr0(f)), s) *)
  | Elocal x, s ->
     (*Format.eprintf "/!\\ %s = ? \n" (string_of x);
     print_ienv "ENV: " env;*)
     let* v = (!_valuation) x env in
     let v = v.cur in
     (*
     Format.eprintf "YES : %s = " (string_of x);
     Pprinter.pp_value_ext v;
     Format.eprintf "\n";
     *)
     return (v, s)
  | Eop(op, e_list), s ->
     (* Format.eprintf "OP\n"; *)
     begin match op, e_list, s with
     | (* initialized unit-delay with a constant *)
       Efby, [_; e2], Stuple [Sval(v); s2] ->
        let* v2, s2 = sexp genv env e2 s2  in
        return (v, Stuple [Sval(v2); s2])
     | Efby, [e1; e2], Stuple [Sopt(v_opt); s1; s2] ->
        let* v1, s1 = sexp genv env e1 s1  in
        let* v2, s2 = sexp genv env e2 s2  in
        (* is-it the first instant? *)
        let v =
          match v_opt with | None -> v1 | Some(v) -> v in
        return (v, Stuple [Sopt(Some(v2)); s1; s2])
     | Eunarypre, [e], Stuple [Sval(v); s] -> 
        Format.eprintf "<PRE>\n";
        Pprinter.pp_value_ext v;
        Pprinter.pp_state s;
        let* ve, s = sexp genv env e s in
        Pprinter.pp_value_ext v;
        Pprinter.pp_state s;
        Format.eprintf "</PRE>\n";
        return (v, Stuple [Sval(ve); s])
     | Eminusgreater, [e1; e2], Stuple [Sval(v); s1; s2] ->
       (* when [v = true] this is the very first instant. [v] is a reset bit *)
       (* see [paper EMSOFT'06] *)
        let* v1, s1 = sexp genv env e1 s1  in
        let* v2, s2 = sexp genv env e2 s2  in
        let* v_out = Initial.ifthenelse v v1 v2 in
        return (v_out, Stuple [Sval(Value(Vbool(false))); s1; s2])
     | Eifthenelse, [e1; e2; e3], Stuple [s1; s2; s3] ->
        let* v1, s1 = sexp genv env e1 s1 in
        let* v2, s2 = sexp genv env e2 s2 in
        let* v3, s3 = sexp genv env e3 s3 in
        let* v = ifthenelse v1 v2 v3 in
        return (v, Stuple [s1; s2; s3])
     | _ -> None
     end
  | Etuple(e_list), Stuple(s_list) ->
     let* v_list, s_list = sexp_list genv env e_list s_list in
     (* check that all component are not nil nor bot *)
     return (strict_tuple v_list, Stuple(s_list))
  (* ça mérite quelques commentaires, pourquoi mettre s à part ? *)
  | Eapp(f, e_list), Stuple (s :: s_list) ->
     let* v_list, s_list = sexp_list genv env e_list (s_list) in
     let* fv = find_gnode_opt f genv in
     let* v, s =
       match fv with
       | CoFun(fv) ->
          let* v_list = fv v_list in 
          let* v = tuple v_list in
          return (v, Sempty)
       | CoNode { step = fv } ->
          let* v_list, s = fv s v_list in
          let* v = tuple v_list in
          return (v, s) in
     return (v, Stuple (s :: s_list)) 
  | Elet(false, eq, e), Stuple [s_eq; s] ->
     let* env_eq, s_eq = seq genv env eq s_eq in
     let env = Env.append env_eq env in
     let* v, s = sexp genv env e s in
     return (v, Stuple [s_eq; s])
  | Elet(true, ({ eq_write } as eq), e), Stuple [s_eq; s] ->
     (* compute a bounded fix-point in [n] steps *)
     let bot = bot_env eq_write in
     let n = size eq in
     let n = if n <= 0 then 0 else n+1 in
     Format.eprintf "<LETREC>\n";
     let* env_eq, s_eq =
        if !only_one_fixpoint = 1
        then seq genv env eq s_eq
        else fixpoint_eq genv env seq eq n s_eq bot
     in
     Format.eprintf "</LETREC>\n";
     (* a dynamic check of causality: all defined names in [eq] *)
     (* must be non bottom provided that all free vars. are non bottom *)
     let _ = causal env env_eq eq_write in
     let env = Env.append env_eq env in
     let* v, s = sexp genv env e s in
     return (v, Stuple [s_eq; s])
  | _ -> Format.eprintf "NONE\n"; None in
  (* Pprinter.pp_exp exp;*)
  stop_at_location e_loc r 4
  (* r *)
  (* /!\ let true/false *)

(* OK *)
and sexp_list genv env e_list s_list =
  match e_list, s_list with
  | [], [] ->
     return ([], [])
  | e :: e_list, s :: s_list ->
     let* v, s = sexp genv env e s in
     let* v_list, s_list = sexp_list genv env e_list s_list in
     return (v :: v_list, s :: s_list)
  | _ ->
     None


(* step function for an equation *)
and seq genv env ({ eq_desc; eq_write; eq_loc } as eq) s =
  let r = match eq_desc, s with 
  | EQeq(p, e), s -> 
     (*
     print_ienv "#1: " env;
     *)
     let* v, s1 = sexp genv env e s in
     (* Format.eprintf "#1bis";
     Pprinter.pp_value_ext v;
     Format.eprintf "\n";*)
     let* env_p1 = matching_pateq p v in
     (*
     print_ienv "#2: " env_p1;
     *)
     Some (env_p1, s1) (* return (env_p, s))) *)
  | EQand(eq_list), Stuple(s_list) ->
     if !set_verbose then Pprinter.pp_equation eq;
     eptf "<AND>\n";
     let* env_eq, s_list = if !eval_method_aux = 1
     then
       begin
         let seq genv acc eq s =
           let* env_eq, s = seq genv acc eq s in
           (*
           print_ienv "ACC1: " acc;
           print_ienv "ENV_EQ: " env_eq;
           *)
           let acc = Env.union
             (fun k v1 v2 -> Some v1)
             env_eq acc in
           (*let* acc = merge env_eq acc in*)
           (*
           print_ienv "ACC2: " acc;
           print_ienv "F   : " (!_f ());

           *)
           return (acc, s)
         in
         let rec mapfold22 f acc xl yl = match xl, yl with
         | [], [] -> return (acc, [])
         | x::xl, y::yl ->
             (* print_ienv "~0 : " acc;*)
             let* acc, s = f acc x y in
             if !set_verbose then begin
               Pprinter.pp_equation x;
               Format.eprintf "\n";
               print_ienv "###\n-> " acc;
             end;
             (* eptf "~2\n";*)
             let* acc, sl = mapfold22 f acc xl yl in
             (* print_ienv "~2 : " acc;*)
             (* eptf "~3\n";*)
             return (acc, s::sl)
         | _ -> None
         in
         mapfold22 (seq genv) env eq_list s_list
       end
     else
       begin
         let seq genv env acc eq s =
           let* env_eq, s = seq genv env eq s in
           if !set_verbose then begin
             Pprinter.pp_equation eq;
             Format.eprintf "\n";
             print_ienv "###\n-> " env_eq;
           end;
           (*print_ienv "ACC1: " acc;
           print_ienv "ENV_EQ: " env_eq;*)
           let* acc = merge env_eq acc in
           (*print_ienv "ACC2: " acc;*)
           return (acc, s)
         in
         mapfold2 (seq genv env) Env.empty eq_list s_list
       end
     in
     (* eptf "</AND>\n";
     print_ienv "~~4 : " env_eq;*)
     return (env_eq, Stuple(s_list))
  | EQlocal(v_list, eq), Stuple [Stuple(s_list); s_eq] ->
     (* Pprinter.pp_equation eq;*)
     let* env', env_local, s_list, s_eq = sblock genv env v_list eq s_list s_eq in
     (* print_ienv "~~5 : " env_local;*)
     if !flattened_fixpoint = 1
     then return (env', Stuple [Stuple(s_list); s_eq])
     else return (env_local, Stuple [Stuple(s_list); s_eq])
  | _ -> None
  in
  (* r *)
  stop_at_location eq_loc r 5


(* block [local x1 [init e1 | default e1 | ],..., xn [...] do eq done *)
and sblock genv env v_list ({ eq_write; eq_loc } as eq) s_list s_eq =
  print_ienv "Block" env;
  let* env_v, s_list =
    Opt.mapfold3
      (svardec genv env) Env.empty v_list s_list (bot_list v_list) in
  let bot =
    if !flattened_fixpoint = 1
    then
      S.fold
      (fun x acc -> if not (Env.mem x acc)
                    then Env.add x P3.vbottom acc
                    else acc)
      eq_write env
    else complete env env_v eq_write
  in
  let n = size eq in
  let n = if n <= 0 then 0 else n+1 in
  eptf "<SBLOCK>\n";
  let* env_eq, s_eq =
    (* on évite les points fixe imbriqués *)
    if !flattened_fixpoint = 1 && !current_local
    then seq genv bot eq s_eq
    else
      begin
      current_local := true;
      let opt = fixpoint_eq genv env seq eq n s_eq bot in
      current_local := false;
      opt
      end
  in
  eptf "</SBLOCK>\n";
  (* a dynamic check of causality for [x1,...,xn] *)
  let _ = causal env env_eq (names env_v) in
  (* store the next last value *)
  let* s_list = Opt.map2 (set_vardec env_eq) v_list s_list in
  (* remove all local variables from [env_eq] *)
  let env = Env.append env_eq env in
  let env_eq = remove env_v env_eq in
  let r = return (env, env_eq, s_list, s_eq) in
  stop_at_location eq_loc r 6
(*
and valuation genv env s_eq eq =
  let sem s_eq env_in =
    let env = Env.append env_in env in
    let* env_out, s_eq = seq genv env eq s_eq in
    let env_out = complete_with_default env env_out in
    return (env_out, s_eq) in
  let dom = S.elements eq.eq_write in
  Lfp5.Make.globeval dom sem s_eq env
*)
(* OK *)
and svardec genv env acc { var_name; var_default; var_loc } s v =
  let* default, s =
    match var_default, s with
    | Ewith_nothing, Sempty -> (* [local x in ...] *)
       return (Val, s)
    | Ewith_init(e), Stuple [si; se] ->
       let* ve, se = sexp genv env e se in
       let* lv =
         match si with
         | Sopt(None) ->
            (* first instant *)
            return (Last(ve))
         | Sval(v) | Sopt(Some(v)) -> return (Last(v))
         | _ -> None in
       return (lv, Stuple [si; se])
    | Ewith_default(e), se ->
       let* ve, se = sexp genv env e se in
       return (Default(ve), se)
    | Ewith_last, Sval(ve) -> (* [local last x in ... last x ...] *)
       return (Last(ve), s)
    | _ -> None in
  let r = return (Env.add var_name { cur = v; default = default } acc, s) in
  stop_at_location var_loc r 7

(* store the next value for [last x] in the state of [vardec] declarations *)
and set_vardec env_eq { var_name; var_loc } s =
  let r = match s with
    | Sempty -> return Sempty
    | Sopt _ | Sval _ ->
       let* v = (!_valuation) var_name env_eq in
       let v = v.cur in
       return (Sval(v))
    | Stuple [_; se] ->
       let* v = (!_valuation) var_name env_eq in
       let v = v.cur in 
       return (Stuple [Sval v; se])
    | _ -> None in
  stop_at_location var_loc r 8

(* remove entries [x, entry] from [env_eq] for *)
(* every variable [x] defined by [env_v] *)
and remove env_v env_eq =
  Env.fold (fun x _ acc -> Env.remove x acc) env_v env_eq

let matching_in env { var_name; var_default } v =
  return (Env.add var_name { cur = v; default = Val } env)

let matching_out env { var_name; var_default } =
  find_value_opt var_name env
  
let funexp genv { f_kind; f_atomic; f_args; f_res; f_body; f_loc } =
  let* si = ieq genv Env.empty f_body in
  let f = match f_kind with
  | Efun ->
     (* combinatorial function *)
     return
       (CoFun
          (fun v_list ->
            let* env =
              Opt.fold2 matching_in Env.empty f_args v_list in
            let* env, _ = seq genv env f_body si in
            let* v_list = Opt.map (matching_out env) f_res in
            return (v_list)))
  | Enode ->
     (* stateful function *)
     (* add the initial state for the input and output vardec *)
     let* s_f_args = Opt.map (ivardec genv Env.empty) f_args in
     let* s_f_res = Opt.map (ivardec genv Env.empty) f_res in
     return
       (CoNode
          { init = Stuple [Stuple(s_f_args); Stuple(s_f_res); si];
            step =
              fun s v_list ->
              match s with
              | Stuple [Stuple(s_f_args); Stuple(s_f_res); s_body] ->
                  (* associate the input value to the argument *) 
                 let* env_args, s_f_args =
                   Opt.mapfold3 (svardec genv Env.empty)
                     Env.empty f_args s_f_args v_list in
                 let* env_res, s_f_res =
                   Opt.mapfold3 (svardec genv env_args)
                     Env.empty f_res s_f_res (bot_list f_res) in
                 print_ienv "Node before fixpoint (args)" env_args;
                 print_ienv "Node before fixpoint (res)" env_res;
                 (* eprint_env env_args; *)
                 let n = Env.cardinal env_res in
                 let n = if n <= 0 then 0 else n+1 in
                 eptf "<BODY>\n";
                 let* env_body, s_body =
                   if !only_one_fixpoint = 1
                   then seq genv env_args f_body s_body
                   else fixpoint_eq genv env_args seq f_body n s_body env_res
                 in
                 eptf "</BODY>\n";
                 (* store the next last value *)
                 print_ienv "Node after fixpoint (body)" env_body;
                 let* s_f_res = Opt.map2 (set_vardec env_body) f_res s_f_res in
                 let* v_list = Opt.map (matching_out env_body) f_res in
                 return (v_list,
                         (Stuple [Stuple(s_f_args); Stuple(s_f_res); s_body]))
              | _ -> None }) in
  stop_at_location f_loc f 10

let exp genv env e =
  let* init = iexp genv env e in
  let step s = sexp genv env e s in
  return (CoF { init = init; step = step })
  
let implementation genv { desc; loc } =
  let r = match desc with
  | Eletdecl(f, e) ->
     (* [e] should be stateless, that is, [step s = v, s] *)
     let* si = iexp genv Env.empty e in
     let* v, s = sexp genv Env.empty e si in
     return (Genv.add (Name(f)) (Gvalue(v)) genv)
  | Eletfundecl(f, fd) ->
     let* fv = funexp genv fd in
     return (Genv.add (Name(f)) (Gfun(fv)) genv)
  (* | Etypedecl(f, td) ->
     return genv *)
  in
  stop_at_location loc r 11
     
let program genv i_list = Opt.fold implementation genv i_list

(* check that a value is causally correct (non bot) *)
(* and initialized (non nil) *)
let not_bot_nil v_list =
  let not_bot_nil v =
    match v with
    | Vbot ->
                    Format.eprintf "Causality error [ARF].@."; raise Stdlib.Exit
    | Vnil ->
       Format.eprintf "Initialization error.@."; raise Stdlib.Exit
    | Value(v) ->
       return v in
  Opt.map not_bot_nil v_list
    
(* run a combinatorial expression *)
(* returns the number of successful iterations *)
let run_fun output fv n =
  let rec runrec i =
    if i = n then i
    else
      let v =
        let* v_list = fv [] in
        let* v_list = not_bot_nil v_list in
        output v_list in
      if Option.is_none v then i
      else runrec (i+1) in
  runrec 0
      
(* run a stream process *)
let run_node output init step n =
  let rec runrec s i =
    if i = n then i
    else
      let v =
        let* v_list, s = step s [] in
        let* v_list = not_bot_nil v_list in
        let* _ = output v_list in
        return s in
      match v with | None -> i | Some(s) -> runrec s (i+1) in
  runrec init 0

(* The main entry function *)
let run genv main ff n =
  let* fv = find_gnode_opt (Name main) genv in
  (* the main function must be of type : unit -> t *)
  let output v_list =
    let _ = Output.pvalue_list_and_flush ff v_list in
    return () in
  match fv with
  | CoFun(fv) ->
     let _ = run_fun output fv n in
     return ()
  | CoNode { init; step } ->
     let _ = run_node output init step n in
     return ()

let check genv main n =
  let* fv = find_gnode_opt (Name main) genv in
  (* the main function must be of type : unit -> bool *)
  let output v =
    if v = [Vbool(true)] then return () else None in
  match fv with
  | CoFun(fv) ->
     let i = run_fun output fv n in
     if i < n then Format.printf "Test failed: only %i iterations.\n" i;
     return ()
  | CoNode { init; step } ->
     let i = run_node output init step n in
     if i < n then Format.printf "Test failed: only %i iterations.\n" i;
     return ()
 

(* *********************************************************************)
(*                                                                     *)
(*                        The ZRun Interpreter                         *)
(*                                                                     *)
(*                             Marc Pouzet                             *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique. All rights reserved. This file is distributed under   *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

let main_node = ref (None: string option)
let set_main s = main_node := Some(s)

let set_check = ref false

let number_of_steps = ref 0
let set_number n = number_of_steps := n

let no_assert = ref false

let set_verbose = ref false
let set_verbose2 = ref false

let set_nocausality = ref true

(* evaluation method *)
let eval_method = ref 2
let set_eval_method em = eval_method := em

(* evaluation results sharing *)
let eval_method_aux = ref 0
let set_eval_method_aux ema = eval_method_aux := ema

(* no fixpoint at root *)
let only_one_fixpoint = ref 0
let fixpoint_count = ref 0
let set_only_one_fixpoint v = only_one_fixpoint := v

(* only one local fixpoint *)
let flattened_fixpoint = ref 0
(* whether already one local fixpoint is being computed *)
let current_local = ref false
let set_flattened_fixpoint v = flattened_fixpoint := v

module Misc =
  struct
    let rec mapfold f acc x_list =
      match x_list with
      | [] -> [], acc
      | x :: x_list ->
         let s, acc = f acc x in
         let s_list, acc = mapfold f acc x_list in
         s :: s_list, acc
  end

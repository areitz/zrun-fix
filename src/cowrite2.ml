open Ast
open Ident
open Misc
open Value

(* in order to respect the usual fixpoint computation hierarchy and thus semantics *)
let is_local { eq_desc } = match eq_desc with
  | EQlocal _ -> true
  | _ -> false

let rec cowrite_eq_aux m ({ eq_desc } as eq) =
  match eq_desc with
  | EQempty -> m
  | EQlocal (vl, eq) -> cowrite_eq_aux m eq
  | EQeq (p, e) ->
      let m = cowrite_exp m e in
      (*
      possibilité de raffiner lorsque len(p) > 1
      pour éviter trop de calculs au moment du
      calcul de plus petit point fixe
      *)
      List.fold_left (fun m k -> Env.add k eq m) m p.desc
  | EQand eql ->
      List.fold_left (fun m e -> cowrite_eq_aux m e) m eql

and cowrite_exp m { e_desc } =
  match e_desc with
  | Econst _ -> m
  | Elocal _ -> m
  | Eop _ -> m
  | Etuple _ -> m
  | Eapp _ -> m
  | Eget _ -> m
  (* correct to ignore the first boolean arg? *)
  | Elet(_, eq, e) ->
      let m = cowrite_eq_aux m eq in
      cowrite_exp m e 

let cowrite_eq = cowrite_eq_aux Env.empty

let printm m =
  if !set_verbose then
    Env.iter
      (fun k e ->
        Format.eprintf "%s : " (string_of k);
        Pprinter.pp_equation e;
        Format.eprintf "\n"
      ) m

let printm2 m =
  if !set_verbose then
    Env.iter
      (fun k e ->
        Format.eprintf "%s : " (string_of k);
        Pprinter.pp_exp e;
        Format.eprintf "\n"
      ) m

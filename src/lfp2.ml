open Ast
open Lfp.Error
open Fix

open Lident

module M = Map.Make(Ident)


module Utils = struct
  let extract_bool = function
  | { e_desc = Econst (Ebool b) } -> b
  | _ -> error ExpectedType
  
  let extract_int = function
  | { e_desc = Econst (Eint x) } -> x
  | _ -> error ExpectedType

  let extract_float = function
  | { e_desc = Econst (Efloat f) } -> f
  | _ -> error ExpectedType

  let return_opt loc r =
    if Option.is_some r
    then Some { e_desc = Option.get r; e_loc = loc }
    else None

  let expected_nb_arguments nb args =
    let args_length = List.length args in
    if args_length < nb then error TooFewArguments
    else
    if args_length > nb then error TooManyArguments
    else
    ()
end

module Builtins = struct
  let builtin_add args =
    Utils.expected_nb_arguments 2 args;
    let args = List.map Utils.extract_int args in
    let r = List.fold_left (+) 0 args in
    Some (Econst (Eint r))

  let builtin_mul args =
    Utils.expected_nb_arguments 2 args;
    let args = List.map Utils.extract_int args in
    let r = List.fold_left ( * ) 1 args in
    Some (Econst (Eint r))
end

module Make = struct
  module V = struct
    type t = Ident.t
  end
  module P = struct
    type t = exp
  end
  module F =
    Fix.ForType(V)(Prop.Option(P))

  let find env id =
    try
      Some (M.find id env)
    with | Not_found -> None

  let rec eval genv exp_opt valuation =
    if Option.is_none exp_opt then None else
    let exp = Option.get exp_opt in
    let exp_to_eval = exp.e_desc in
    let exp_loc = exp.e_loc in

    match exp_to_eval with
    | Econst c -> exp_opt
    | Elocal id -> valuation id
    | Eapp (op, el) ->
        let el = List.map (fun exp -> Some exp) el in
        let el = List.map (fun exp -> eval genv exp valuation) el in
        if List.exists Option.is_none el
        then None
        else
          let el = List.map (Option.get) el in
          begin match (modname op) with
          | "+" -> Utils.return_opt exp_loc (Builtins.builtin_add el)
          | "*" -> Utils.return_opt exp_loc (Builtins.builtin_mul el)
          | s -> call s genv el exp_loc
          end
    (* tuples have been previously "flattened" in dot.ml,
    manually *)
    | Etuple _ -> error NestedTuples
    (* let intermediate equations have also been previously "flattened" in dot.ml,
    using the map m ident -> exp *)
    | Elet (_, _, exp) ->
        let e = Some exp in
        eval genv e valuation

  and call s genv actual_args exp_loc =
    let ext_env, formal_args, ext_res  = List.assoc s genv in
    let ext_env  = List.fold_left2 (fun m k v -> M.add k v m) ext_env formal_args actual_args in
    let ext_valuation = globeval genv ext_env in
    match (List.length ext_res) with
    | 0 -> None
    | 1 -> ext_valuation (List.hd ext_res)
    | n -> let res_opt_list = List.map ext_valuation ext_res in
           if List.exists Option.is_none res_opt_list
           then None
           else
             Utils.return_opt exp_loc (Some (Etuple (List.map Option.get res_opt_list)))
    

  and globeval (genv : ((string * (exp M.t * Ident.t list * Ident.t list)) list)) (env : exp M.t)  =
    F.lfp (fun id -> eval genv (find env id))
    
end


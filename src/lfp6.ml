open Fix

open Ident
open Value
open Common
open Debug

module Make = struct
  module V = struct
    type t = Ident.t
  end
  module M = Glue.PersistentMapsToImperativeMaps(Env)

  let i = ref 0

  let globeval dom sem s_eq =
    let module F2 = Fix.Make(M)(P3) in
    let env = ref Env.empty in
    let getenv () = env in
    F2.lfp
      (fun id valuation ->
        (* Format.eprintf "<VAL>\n"; *)
        (* incr i; *)
        (* Format.eprintf "%i : %s\n" (!i) (string_of id); *)
        let env = !env in
        print_ienv "ENV! " env;
        let s = sem s_eq env id in
        if Option.is_some s
        then
          begin
            let env2 = fst (Option.get s) in
            print_ienv "~~6 : " env2;
            let env3 = Env.union (fun k v1 v2 -> Some v1) env2 env in
            let renv = getenv () in
            renv := env3;
            (* valuation à la place ? *)
            (*Format.eprintf "1/2\n";*)
            let v = Env.find id env3 in
            (*Format.eprintf "2/2\n";*)
            (* Format.eprintf "</VAL>\n"; *)
            v
          end
        else
          failwith "Not found"
      ), getenv
end

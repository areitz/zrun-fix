
module P2 = struct
  type pvalue =
  | Vbot
  | Vvoid
  | Vint of int
  | Vbool of bool
  | Vfloat of float
  | Vtuple of pvalue list 
  | Vchar of char
  | Vstring of string

  let bottom = Vbot

  let rec equal v1 v2 = match (v1, v2) with
  | Vbot, Vbot -> true
  | Vvoid, Vvoid -> true
  | Vint i1, Vint i2 when i1 = i2 -> true
  | Vbool b1, Vbool b2 when b1 = b2 -> true 
  | Vfloat f1, Vfloat f2 when f1 = f2 -> true
  | Vtuple l1, Vtuple l2 -> List.fold_left (&&) true (List.map2 equal l1 l2)
  | Vchar c1, Vchar c2 when c1 = c2 -> true
  | Vstring s1, Vstring s2 when s1 = s2 -> true
  | _, _ -> false

  let is_maximal = function
  | Vbot -> false
  | _ -> true

  type t = pvalue

  type property = pvalue
end


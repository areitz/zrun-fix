(* *********************************************************************)
(*                                                                     *)
(*                        The ZRun Interpreter                         *)
(*                                                                     *)
(*                             Marc Pouzet                             *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique. All rights reserved. This file is distributed under   *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(* the main *)
open Misc
open Monad.Opt
open Location
open Pprinter
open Main
(*open Graph*)
(*open Coiter3*)

let lexical_error err loc =
  Format.eprintf "%aIllegal character.@." output_location loc;
  raise Stdlib.Exit

let syntax_error loc =
  Format.eprintf "%aSyntax error.@." output_location loc;
  raise Stdlib.Exit

let parse parsing_fun lexing_fun source_name =
  let ic = open_in source_name in
  let lexbuf = Lexing.from_channel ic in
  lexbuf.Lexing.lex_curr_p <-
    { lexbuf.Lexing.lex_curr_p with Lexing.pos_fname = source_name };
  try
    parsing_fun lexing_fun lexbuf
  with
  | Lexer.Lexical_error(err, loc) ->
     close_in ic; lexical_error err loc
  | Parser.Error ->
     close_in ic;
     syntax_error
       (Loc(Lexing.lexeme_start lexbuf, Lexing.lexeme_end lexbuf))

let parse_implementation_file source_name =
  parse Parser.implementation_file Lexer.main source_name

(* evaluate the main node [main] given by option [-s] for [n] steps *)
(* with check *)

let eval1 source_name =
  let p = parse_implementation_file source_name in
  Debug.print_message "Parsing done";
  let p = Scoping.program p in
  Debug.print_message "Scoping done";
  p

let eval2 main number check eval_method p =
  match eval_method with
(*
  | 0 | 1 ->
      let gms = Dot.mkg_program p in
      let genv = List.map (fun (s, ((g, m), a, r)) -> (s, (m, a, r))) gms in
      let main = if Option.is_none main
      then fst (List.hd (List.rev genv))
      else Option.get main
      in
      Debug.print_message ("Main node: " ^ main);
      let env, _, _ = List.assoc main genv in
      begin match eval_method with
      | 0 -> Dot.execute genv env
      | 1 -> Dot.execute2 genv env
      end
*)
  | 8 ->
     (*set_verbose := true;*)
     let p = Normalize.normalize_p p in
     (*Pprinter.pp_program p;*)
     let genv = Eval8.eval_p p in
     begin match main with
     | None -> ()
     | Some(main) -> let _ = Eval8.runfind genv main [] in ()
     end
   | 9 ->
     (*set_verbose := true;*)
     (*Format.eprintf "EVAL 9\n";*)
     let p = Normalize.normalize_p p in
     (*Pprinter.pp_program p;*)
     let genv = Eval9.eval_p p in
     begin match main with
     | None -> ()
     | Some(main) -> let _ = Eval9.runfind genv main [] in ()
     end
    | 10 ->
     (*set_verbose := true;*)
     (*Format.eprintf "EVAL 10\n";*)
     let p = Normalize.normalize_p p in
     (*Pprinter.pp_program p;*)
     let genv = Evalx.eval_p p in
     begin match main with
     | None -> ()
     | Some(main) -> let _ = Evalx.runfind genv main [] in ()
     end
     | 11 ->
     (*set_verbose := true;*)
     (*Format.eprintf "EVAL 11\n";*)
     (*let p = Normalize.normalize_p p in*)
     (*Pprinter.pp_program p;*)
     let p = Cowrite3.write_program p in
     (*Format.eprintf "EVAL 11\n";*)
     let genv = Evalxi.eval_p p in
     begin match main with
     | None -> ()
     | Some(main) -> let _ = Evalxi.runfind genv main [] in ()
     end
     | 12 ->
     (*set_verbose := true;*)
     (*Format.eprintf "EVAL 12\n";*)
     (*Pprinter.pp_program p;*)
     let p = Cowrite3.write_program p in
     let genv = Evalxii.eval_p p in
     begin match main with
     | None -> ()
     | Some(main) -> let _ = Evalxii.runfind genv main [] in ()
     end
     | 13 ->
     (*set_verbose := true;*)
     (*Format.eprintf "EVAL 13\n";*)
     (*Pprinter.pp_program p;*)
     let p = Cowrite3.write_program p in
     let genv = Evalxiii.eval_p p in
     begin match main with
     | None -> ()
     | Some(main) -> let _ = Evalxiii.runfind genv main [] number in ()
     end
     (*| 14 ->
     let p = Cowrite3.write_program p in
     let genv = Eval_lazy.eval_p p in
     begin match main with
     | None -> ()
     | Some(main) -> let _ = Eval_lazy.runfind genv main [] number in ()
     end*)
     | 15 ->
     let p = Cowrite3.write_program p in
     let _ =
     let* genv = Eval15.program Initial.genv0 p in
     Debug.print_message "Evaluation of definitions done";
     let* r =
     begin match main with
     | None -> return ()
     | Some main ->
         Debug.print_message "The main node exists";
         let* r = 
           (* make [n] steps and checks that every step returns [true] *)
           if check then begin Coiteration.check genv main number end
           else begin
             (* make [n] steps *)
             Coiteration.run genv main Format.std_formatter number end in
         return r
     end
     in
     return r
     in ()
     
      

  | 2 | 3 | 4 | 5 | 6 | 7 ->
      let p = Write.program p in
      Debug.print_message "Write done";
      let _ =
      let* genv = Coiteration.program Initial.genv0 p in
      Debug.print_message "Evaluation of definitions done";
      let* r =
      begin match main with
      | None -> return ()
      | Some(main) ->
         Debug.print_message "The main node exists";
         let* r =
           (* make [n] steps and checks that every step returns [true] *)
           if check then begin Coiteration.check genv main number end
           else begin
             (* make [n] steps *)
             Coiteration.run genv main Format.std_formatter number end in
         return r
      end
      in
      return r
      in ()
  ;
  ()

  (* Dot.print_graphs basename gs; *)
  (* let _ = Pprinter.pp_program p in (); *)
  (* flush stdout; *)
let eval filename =
  if Filename.check_suffix filename ".zls"
  then
    begin
      Location.initialize filename;
      let p = eval1 filename in
      let _ = eval2 !main_node !number_of_steps !set_check !eval_method p in ()
    end
  else raise (Arg.Bad "Expected *.zls file.")


let doc_main = "\tThe main node to evaluate"
let doc_number_of_steps = "\tThe number of steps"
let doc_check = "\tCheck that the simulated node returns true"
let doc_verbose = "\tVerbose mode"
let doc_verbose2 = "\tVerbose mode 2"
let doc_no_assert = "\tNo check of assertions"
let doc_nocausality = "\tTurn off the check that are variables are non bottom"

let doc_eval_method = "\tUse an eval method among simple loop/fix fixpoint library"
let doc_eval_method_aux = "\tShare information acquired during the same iteration"
let doc_only_one_fixpoint = "\tNo global fixpoint at root"
let doc_flattened_fixpoint = "\tOnly one fixpoint for local blocks"

let errmsg = "Options are:"

(*
notation : (x, y, z)
x = eval_method
y = eval_method_aux
z = only_one_fixpoint

- zrun x = 2
-> (2, 0, 1) -> exactement k * n zrun sans partage, benchs ok sur permutations
-> (2, 0, 0) -> exactement k * n * 2 idem, idem (x2 avec le point fixe de départ ajouté)
-> (2, 1, 1) -> en moyenne k * (n+1)/2 zrun avec partage, benchs ok sur permutations
-> (2, 1, 0) -> en moyenne k * (n+1) idem, idem (x2 avec le point fixe de départ ajouté)
- zrun x = 4,5,6,7
-> (4, _, _) -> causality error
-> (5, _, _) -> requiert fix patché
-> (6, _, _) -> OK, à bench
- mem x = 7
-> (7, 0, 0) ~ (7, 0, 1) en terme de temps (n = 100, k = 1000)
-> (7, 1, _) ~ (7, 0, _) par ailleurs (logique)
-> 1.33s
- fix x = 6 (cowrite)
-> (6, 0, 0) ~ (6, 0, 1)
-> (6, 1, _) ~ (6, 1, 0)
-> 1.34s
- faire des régressions linéaires O(n log n) à vue de pied
-> 8 incoming

###
reprendre le plan
- relire Marc
- refs sur les interprète, au moins regarder Pottier
- fast interpreter in OCaml

- vérifier les last
- (7, _, _, 1)

deux pdv
- full verif
- full tests

Z) intro
A) algos généraux et complexité
- zc
- zp
- fix
- mem
- [~OK] /!\ 1) dataflow
  - aussi complet que possible
- [SKIP] /!\ custom avec schedule
B) optis locales
- [~OK] /!\ 2) un seul point fixe -> pas gênant grâce au scoping et imbrication
- [sans code] /!\ 3) vérification convergence point fixe forall non bot pour éviter une itération en plus
- /!\ ? valeurs de retour, y a des choses à faire...
C) autre point de vue sur la causalité
D) version opti
E) évaluation des performances
- permutations
- ?? /!\ génération de programmes aléatoires avec AFL ?
- /!\ génération de programmes aléatoires aussi uniforme que possible
- distribution uniforme + extrême pour résultats représentatifs ?
F) perspectives
- zelus
- velus
- du test du test du test
- ?? asynchronous enveloppé dans du synchronous avec OCaml multicore ?
  - criticable car si différentes composantes connexes alors différentes fonctions pour modularité
G) conclusion
###
- [SKIP] adapter pprinter pour retourner string
- [SKIP] écrire dans fichier pour pouvoir évaluer programmes aléatoires
- [OK] un seul point fixe
- [OK] gérer retour
- [OK] x = 2 bench
- [SKIP] vérification convergence point fixe à améliorer par un forall non bot
- [OK] un seul point fixe avec exemples d'imbrication
  -> ne marche qu'avec x = 2
- [SKIP?] gestion des n-uplets en valeurs de retours d'appels
- [OK~] /!\/!\ version optimisée avec schedule (+ vérification de la causalité custom)
  a priori pas de souci <- amélioration vérification causalité en longueur de fils pour 6/7
  - appels
  - complétude
  - initial.ml
  - racine

- [plus tard] complexifier les choses avec randdag/rand.planar (a priori rand bof, zéro garantie acyclique)

- benchs
  - x = 4,5,6,7 bench avec régressions
  - écrire versions indépendantes -> remplacer ref par static pour benchs
- mettre au propre expériences
- décommenter, minimiser diff, soigner code
- rapport rapport rapport

- [SKIP?] écrire algo efficace pour calcul de C avec union-find de Filliatre
  -> algo en temps linéaire avec tableaux, espace n = ok
  -> algo en temps moyen linéaire avec tableaux, tables de hachage, espace en moyenne n+1/2
- [plus tard] vérification de la causalité à vérifier + exemple de Malik
- [plus tard] flattened_fixpoint avec x \neq 2
*)
(*
module Label = struct
  type t = int
end
module G1 = Persistent.Digraph.Abstract(Label)
module B = Graph.Builder.P(G1)
*)

(*module DotParser =
  Graph.Dot.Parse
  (B)
  (struct
    let node (id, _) _  =
        match id with
        | Graph.Dot_ast.Ident s 
        | Graph.Dot_ast.Number s
        | Graph.Dot_ast.String s
        | Graph.Dot_ast.Html s -> (int_of_string s)
        let edge _ = ()
  end)*)

(*open Core*)
let main2 () =
  (**)
  (*set_verbose := false;*)
  (**)
  Random.self_init ();

  (*let ch = open_in "temp.dot" in*)
  (*let sl = Core.In_channel.read_lines "temp.dot" in
  (*let s = really_input_string ch (in_channel_length ch) in*)
  (*let s = List.fold_left (fun acc s -> acc ^ s) "" sl in*)
  (*let concat = fun a b -> a ^ b in*)
  let s = List.fold_left (^) "" sl in
  let g = DotParser.parse s in ();*)


  set_verbose := false;
  (**)
  let n = 100 in
  let k = 0 in
  let iter = 100 in
  let perms = Main.random_permutations k n in
  (*let perms = [List.init n (fun k -> k+1) ] in*)
  let pgms = List.map (Main.genprogram n) perms in
  (*Pprinter.pp_program (List.hd pgms);*)
  eval_method := 15;
  (*eval_method_aux := 1;
  only_one_fixpoint := 0;
  flattened_fixpoint := 0;*)
  List.iter (eval2 (Some "main") iter !set_check !eval_method) pgms;
  (*print_endline ("fxpc: " ^ (string_of_int (!fixpoint_count)));*)
  (*print_endline (string_of_int (List.length perms));*)
  (*print_endline (string_of_int (List.length pgms));*)
  (**)
  (*
  set_verbose := true;
  let n = 3 in
  let perm = Main.random_permutation n in
  let perm2 = List.init n (fun k -> k+1) in
  let p = Main.genprogram n perm in
  List.iter Pprinter.pp_program [p];
  eval_method := 2;
  eval_method_aux := 0;
  only_one_fixpoint := 1;
  let _ = eval2 (Some "main") 1 !set_check 2 p in ();
  print_endline ("fxpc: " ^ (string_of_int (!fixpoint_count)));
  *)
  (* let g = parse_dot_file "~/balsa/randdag/temp" in *)
  (*
  let k = 1000 in
  let n = 50 in
  let ps = random_permutations k n in
  let pl = List.map
  (fun p -> let a,b,c = Bench.scan p in List.length b)
  ps in

  let s = List.fold_left (+) 0 pl in
  print_endline (string_of_float ((float_of_int s) /. (float_of_int k)));
  (**)
  *)
  ()

let main () =
  main2 ();
  try
    Arg.parse (Arg.align
                 [ "-s", Arg.String set_main, doc_main;
                   "-n", Arg.Int set_number, doc_number_of_steps;
                   "-check", Arg.Set set_check, doc_check;
                   "-v", Arg.Set set_verbose, doc_verbose;
                   "-v2", Arg.Set set_verbose2, doc_verbose2;
                   "-noassert", Arg.Set no_assert, doc_no_assert;
                   "-nocausality", Arg.Set set_nocausality, doc_nocausality;
                   "-evalmethod", Arg.Int set_eval_method, doc_eval_method;
                   "-evalmethodaux", Arg.Int set_eval_method_aux, doc_eval_method_aux;
                   "-onlyonefixpoint", Arg.Int set_only_one_fixpoint, doc_only_one_fixpoint;
                   "-flattenedfixpoint", Arg.Int set_flattened_fixpoint, doc_flattened_fixpoint])
      eval
      errmsg
  with
  | Scoping.Error | Stdlib.Exit -> exit 2

let _ = main ()
let _ = exit 0


open Fix

open Ident
open Value
open Common
open Debug

module Make = struct
  module V = struct
    type t = Ident.t
  end
  module MV = Env
  module M = Glue.PersistentMapsToImperativeMaps(MV)
  (* module F2 = Fix.ForType(V)(P3) *)
  (* module F2 = Fix.Make(M)(P3) *)

  (* let (getp : unit -> P3.t Env.t ref) = F2.getp *)

  let i = ref 0

  let globeval dom sem s_eq =
    let module F2 = Fix.Make(M)(P3) in
    let env = ref Env.empty in
    let getenv () = env in
    F2.lfp
      (fun id valuation ->
        incr i;
        Format.eprintf "%i : %s\n" (!i) (string_of id);
        let env = !env in
        
        (*let rt = F2.gett () in
        Format.eprintf "<SEM RT : %i ; %i>\n" (!i) (Env.cardinal !rt);
        print_ienv "RT: " !rt;
        let rp = F2.gett () in
        Format.eprintf "<SEM RP : %i ; %i>\n" (!i) (Env.cardinal !rp);
        print_ienv "RP: " !rp;
        let env2 = Env.union (fun k v1 v2 -> Some v1) !rp !rt in
        let env = Env.union (fun k v1 v2 -> Some v1) env2 env in*)
        print_ienv "ENV! " env;
        (*
        let env = List.fold_left
          (fun env id ->
               let v = valuation id in
               Env.add id v env
          )
          env dom
        in
        *)
        let s = sem s_eq env in
        (*Format.eprintf "</SEM RT : %i ; %i>\n" (!i) (Env.cardinal !rt);
        Format.eprintf "</SEM RP : %i ; %i>\n" (!i) (Env.cardinal !rp);*)
        if Option.is_some s
        then
          begin
            let env2 = fst (Option.get s) in
            print_ienv "~~6 : " env2;
            (*
            ()
            *)
            (*
            let env3 = Env.union (fun k v1 v2 -> Some v1) !rp !rt in
            let env4 = Env.union (fun k v1 v2 -> Some v1) env3 env2 in
            *)
            let renv = getenv () in
            renv := env2;
            Env.find id env2
          end
        else
          begin
            Env.find id env
          end
      ), getenv
  (* let init () = (getp, globeval) *)
end
        (* Format.eprintf "#2 : %s\n" (string_of id); *)
        (* let env2 =
          (*
          if !s then begin s := false;
          *)
          (* if Env.cardinal env != List.length dom then begin *)
          (*
          *)
          List.fold_left
          (fun env id ->
               let v = valuation id in
               Env.add id v env
          )
          env dom
          (*
          end else env
          *)
        in
        *)
        (* let env_f = !(F2.getp ()) in *)
        (*
        Format.eprintf "%i\n" (Env.cardinal env);
        Format.eprintf "%i\n" (Env.cardinal env2);
        Format.eprintf "%i\n" (Env.cardinal env_f);
        *)
        (*let env_f = (F.getp P3.bottom :> 'a Env.t) in*)
        (* let (sgetp : P3.t -> P3.t Ident.Env.t) = fun x -> F.getp x in *)
        (* let env_f = (sgetp P3.bottom :> 'a Env.t) in *)
        (*let env_f = (F2.getp () :> P3.t M.t) in*)
        (*
        print_ienv "#1: " env;
        print_ienv "#2: " env2;
        print_ienv "#F: " env_f;
        *)
        

(*
        let env3 = MV.union
          (fun k v1 v2 -> Some v1)
          env !env_f
        in
        let env4 = MV.union
          (fun k v1 v2 -> Some v1)
        env3 env2
  in
*)
        (* Format.eprintf "#1 : %s\n" (string_of id); *)
 

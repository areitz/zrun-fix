open Ast
open Ident
open Misc
open Value

(* in order to respect the usual fixpoint computation hierarchy and thus semantics *)
let is_local { eq_desc } = match eq_desc with
  | EQlocal _ -> true
  | _ -> false

let rec cowrite_eq_aux m ({ eq_desc } as eq) s =
  match eq_desc, s with
  | EQempty, Sempty -> m
  | EQlocal (vl, eq), Stuple [Stuple sl; s_eq] -> cowrite_eq_aux m eq s_eq
  | EQeq (p, e), s ->
      let m = cowrite_exp m e s in
      (* let m = cowrite_exp m e in, cf src/dot *)
      (*
      possibilité de raffiner lorsque len(p) > 1
      pour éviter trop de calculs au moment du
      calcul de plus petit point fixe
      *)
      List.fold_left (fun m k -> Env.add k (eq, s) m) m p.desc
  | EQand eql, Stuple sl ->
      List.fold_left2 (fun m e s -> cowrite_eq_aux m e s) m eql sl

and cowrite_exp m { e_desc } s =
  match e_desc, s with
  | Econst _, _ -> m
  | Elocal _, _ -> m
  | Eop _, _ -> m
  | Etuple _, Stuple _ -> m
  | Eapp _, Stuple _ -> m
  (* correct to ignore the first boolean arg? *)
  | Elet(_, eq, e), Stuple [s_eq; s] ->
      let m = cowrite_eq_aux m eq s_eq in
      cowrite_exp m e s 

let cowrite_eq = cowrite_eq_aux Env.empty

let printm m =
  if !set_verbose then
    Env.iter
      (fun k (e, s) ->
        Format.eprintf "%s : " (string_of k);
        Pprinter.pp_equation e;
        Format.eprintf "\n"
      ) m

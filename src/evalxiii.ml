open Ident
open Ident2
open Cowrite3
open Ast
open Value
open Misc
open Initial
open Fix

let prints s =
  if !set_verbose then
  Env.iter
    (fun k v ->
       Format.eprintf "%s :" (string_of k);
       S.iter (fun s -> Format.eprintf " %s ;" (string_of s)) v;
       Format.eprintf "\n")
     s

let printv k v =
  if !set_verbose then begin
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v;
  Format.eprintf "\n";
  end

let printenv msg env =
  if !set_verbose then begin
  Format.eprintf "%s\n" msg;
  Env.iter (fun k v ->
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v)
  env;
  Format.eprintf "\n" end

let print s =
  if !set_verbose then Format.eprintf "%s" s

let printvv k v =
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v;
  Format.eprintf "\n"

let vardec_to_ident { var_name } = var_name

let value v = match v with
| Eint i -> Vint i
| Ebool b -> Vbool b
| Efloat f -> Vfloat f
| Evoid -> Vvoid
| Estring s -> Vstring s
| Echar c -> Vchar c

let rec iexp genv { e_desc } =
  let r = match e_desc with
  | Econst _ | Elocal _ -> Sempty
  | Eop (Eifthenelse, [e1; e2; e3]) ->
      let s1 = iexp genv e1 in
      let s2 = iexp genv e2 in
      let s3 = iexp genv e3 in
      Stuple [s1; s2; s3]
  | Eop (Efby, [e1; e2]) ->
      let s1 = iexp genv e1 in
      let s2 = iexp genv e2 in
      Stuple [s1; s2]
  | Eop (Eunarypre, [e]) ->
      let s = iexp genv e in
      let s = Stuple [Sval Vnil; s] in
      (*Format.eprintf "PRE\n";
      Pprinter.pp_state s;*)
      s
  | Eop (Eminusgreater, [e1; e2]) ->
      let s1 = iexp genv e1 in
      let s2 = iexp genv e2 in
      let s = Stuple [Sval (Value (Vbool true)); s1; s2] in
      (*Format.eprintf "MG\n";
      Pprinter.pp_state s;*)
      s
  | Etuple e_list ->
      let s_list = List.map (iexp genv) e_list in
      Stuple s_list
  | Elet(_, eq, e) ->
      let s_eq = ieq genv eq in
      let se = iexp genv e in
      Stuple [s_eq; se]
  | Eapp (f, e_list) ->
      let s_list = List.map (iexp genv) e_list in
      let fv = try Genv.find f genv with Not_found -> failwith "not found in genv" in
      let s =
        match fv with
        | Comb _ -> Sempty
        | Seq (s, _) -> s in
      Stuple (s::s_list)
  in r
and ieq genv { eq_desc } =
  let r = match eq_desc with
  | EQeq (_, e) -> iexp genv e
  | EQand eql ->
      let seql = List.map (ieq genv) eql in
      Stuple seql
  | EQlocal (_, eq) -> ieq genv eq
  | EQempty -> Sempty
  | EQassert e -> iexp genv e
  | EQif (e, eq1, eq2) ->
      let se = iexp genv e in
      let seq1 = ieq genv eq1 in
      let seq2 = ieq genv eq2 in
      Stuple [se; seq1; seq2]
  in r

module Make = struct
  module M = Glue.PersistentMapsToImperativeMaps(Env2)
    let i = ref 0
    let globeval cowritem init_env genv body opt_s =
      let module F2 = Fix.Make(M)(P6) in
      let smap = if Option.is_some opt_s
                 then associate_eq Env2.empty body (Option.get opt_s)
                 else Env2.empty in
      (*printmeqs smap;*)
      (*if !i > 10 then failwith "STOP";*)
      F2.lfp
        (fun main_eq valuation ->
  (*Pprinter.pp_equation main_eq;
  Format.eprintf "\n";*)
let rec eval_exp env { e_desc } s : value * value Env.t * state =
  let r = match e_desc, s with
  | Econst v, s -> Value (value v), env, s
  | Elocal x, s ->
     incr i;
     (*for j = 0 to !i-1; do Format.eprintf "  "; done;
     Format.eprintf "<<< %s\n"(string_of x);*)
     let v = if Env.mem x init_env
     then Env.find x init_env
     else
       let eql = Env.find x cowritem in
       let env = List.fold_left
         (fun acc_env eq ->
           let env, _, _ = valuation eq in
           Env.union (fun k v1 v2 -> Some v2) acc_env env)
         env eql in
       (*Format.eprintf "--> %s : " (string_of x);
       Pprinter.pp_equation e; Format.eprintf "\n";*)
       (*let env, _ = valuation e in*)
       if Env.mem x env
       then Env.find x env
       else (*begin Format.eprintf "%s\n" (string_of x);*) Vbot
     in
     (*for j = 0 to !i-1; do Format.eprintf "  "; done;
     Format.eprintf ">>> %s : " (string_of x);
     decr i;
     Pprinter.pp_value_ext v;
     Format.eprintf "\n";*)
     v, env, s
  | Eop (Eifthenelse, [e1; e2; e3]), Stuple [s1; s2; s3] ->
      let v1, env1, s1 = eval_exp env e1 s1 in
      let (Value (Vbool b1)) = v1 in
      (*if b1
      then eval_exp e2
      else eval_exp e3*)
      let v2, env2, s2 = eval_exp env e2 s2 in
      let v3, env3, s3 = eval_exp env e3 s3 in
      let v, env = if b1 then v2, env2 else v3, env3 in
      v, env, Stuple [s1; s2; s3]
  | Eop (Efby, [_;e2]), Stuple [Sval v; s2] ->
      let v2, env, s2 = eval_exp env e2 s2 in
      v, env, Stuple [Sval v2; s2]
  | Eop (Eunarypre, [e]), Stuple [Sval v; s] ->
      (*Format.eprintf "<PRE>\n";
      Pprinter.pp_value_ext v;
      Pprinter.pp_state s;*)
      let ve, env, s = eval_exp env e s in
      (*Pprinter.pp_value_ext ve;
      Pprinter.pp_state s;
      Format.eprintf "</PRE>\n";*)
      v, env, Stuple [Sval ve; s]
  | Eop (Eminusgreater, [e1;e2]), Stuple [Sval v; s1; s2] ->
      let v1, env1, s1 = eval_exp env e1 s1 in
      let v2, env2, s2 = eval_exp env e2 s2 in
      let Value (Vbool vb) = v in
      let v, env = if vb then v1, env1 else v2, env2 in
      v, env, Stuple [Sval (Value (Vbool false)); s1; s2]
  | Eapp (f, e_list), Stuple (s :: s_list) ->
      (*Format.eprintf "EAPP %i %i\n" (List.length e_list) (List.length s_list);
      List.iter (fun v -> Pprinter.pp_state v) s_list;*)
      let v_list, env, s_list = List.fold_left
        (fun (v_acc, env, s_acc) (e, s) ->
          let v, env, s = eval_exp env e s
          in v::v_acc, env, s::s_acc)
        ([], env, [])
        (List.combine e_list s_list) in
      let v_list = List.rev v_list in
      let s_list = List.rev s_list in
      let fv =
        try Genv.find f genv
        with
        | Not_found -> failwith "Not found in genv" in
      let res, s = match fv with
      | Comb fv -> fv v_list, Sempty
      | Seq (_, fv) -> fv s v_list
      in
      let s = Stuple (s :: s_list) in
      if List.length res = 1
      then List.hd res, env, s
      else Value (Vtuple res), env, s
  (* TODO : take rec into account *)
  | Elet (_, eq, e), Stuple [s_eq; s] ->
      (*Pprinter.pp_equation eq; Format.eprintf "\n";
      Pprinter.pp_exp e; Format.eprintf "\n";
      printenv "#1" env;*)
      let env, s_eq = eval_eq env eq s_eq in
      (*printenv "#2" env;*)
      let v, env, s = eval_exp env e s in
      (*printenv "#3" env;*)
      v, env, Stuple [s_eq; s]
  | Eget (i, e), s ->
      let v, env, s = eval_exp env e s in
      let Value (Vtuple v_list) = v in
      List.nth v_list i, env, s
  | Etuple e_list, Stuple s_list ->
      Format.eprintf "ETUPLE\n";
      let v_list, env, s_list = List.fold_left
        (fun (v_acc, env, s_acc) (e, s) ->
          let v, env, s = eval_exp env e s
          in v::v_acc, env, s::s_acc)
        ([], env, [])
        (List.combine e_list s_list) in
      let v_list = List.rev v_list in
      let s_list = List.rev s_list in
      Value (Vtuple v_list), env, Stuple s_list
  in r
and eval_eq env ({ eq_desc } as eq) s : value Env.t * state =
  let r = match eq_desc, s with
  | EQeq (p, e), s ->
      (*Pprinter.pp_equation eq;
      Pprinter.pp_state s;*)
      (*incr i;
      for j = 0 to !i-1; do Format.eprintf "  "; done;
      *)
      (*Format.eprintf "<<< %s\n"(string_of (List.hd p.desc));*)

      let v, env, s = eval_exp env e s in
      (* to be replaced by coiteration matching_pateq *)
      if List.length p.desc = 1
      then
        let id = List.hd p.desc in
        Env.singleton id v, s
      else
        let Value (Vtuple vl) = v in
        let env = List.fold_left2
          (fun acc k v -> Env.add k v acc)
          env
          p.desc
          vl in
        env, s
  | EQlocal (_, eq), s -> eval_eq env eq s
  | EQempty, s -> env, s
  | EQand eql, Stuple s_list ->
      (* Format.eprintf "EAND %i %i\n" (List.length eql) (List.length s_list);*)
      let env, s_list = List.fold_left
        (fun (acc_env, s_acc) (eq, s) ->
           let env, s = eval_eq env eq s in
           (* on respecte l'ordre du programm *)
           Env.union (fun k v1 v2 -> Some v2) acc_env env,
           s::s_acc)
        (env, [])
        (List.combine eql s_list) in
      env, Stuple (List.rev s_list)
  | EQif (eb, eq1, eq2), Stuple [seb; s_eq1; s_eq2] ->
      let vb, env, seb = eval_exp env eb seb in
      let env1, s_eq1 = eval_eq env eq1 s_eq1 in
      let env2, s_eq2 = eval_eq env eq2 s_eq2 in
      let Value (Vbool b) = vb in
      (if b then env1 else env2), Stuple [seb; s_eq1; s_eq2]
  | EQassert e, s ->
      let e, env, s = eval_exp env e s in
      let Value (Vbool b) = e in
      if b then env, s else failwith "assertion failed"

      (*for j = 0 to !i-1; do Format.eprintf "  "; done;
      Format.eprintf ">>> %s : " (string_of id);
      decr i;
      Pprinter.pp_value_ext v;
      Format.eprintf "\n";*)
  | _ -> failwith "not implemented"
  in r
in
let env, dom, s = 
if Option.is_some opt_s
then
  let s = try Env2.find main_eq smap with | Not_found -> Pprinter.pp_equation main_eq;
        failwith "arf" in
  let env, s = eval_eq Env.empty main_eq s in
  env, main_eq.eq_write, s
else
  let s = ieq genv main_eq in
  let env, _ = eval_eq Env.empty main_eq s in
  env, main_eq.eq_write, s
in
(*Format.eprintf "returning: \n";
printenv "env" env;
Pprinter.pp_state s;*)
env, dom, s)
end

let run_comb genv fv args =
  let name, fexp = fv
  in
  let init_env = List.fold_left2
    (fun acc k v -> Env.add k v acc)
    Env.empty
    (List.map vardec_to_ident fexp.f_args)
    args
  in
  let cowritem = cowrite fexp.f_body in

  let res = List.map vardec_to_ident fexp.f_res in
  let module M = Make in
  let solution = M.globeval cowritem init_env genv fexp.f_body None in
  let env, _, s = solution fexp.f_body in
  let vres = List.map
    (fun id ->
    try Env.find id env with | Not_found -> Vbot)
    res in
  vres

let run_seq genv fv args s =
  let name, fexp = fv
  in
  let init_env = List.fold_left2
    (fun acc k v -> Env.add k v acc)
    Env.empty
    (List.map vardec_to_ident fexp.f_args)
    args
  in
  let cowritem = cowrite fexp.f_body in

  let res = List.map vardec_to_ident fexp.f_res in
  let module M = Make in
  let solution = M.globeval cowritem init_env genv fexp.f_body (Some s) in
  let env, _, s = solution fexp.f_body in
  let vres = List.map
    (fun id ->
    try Env.find id env with | Not_found -> Vbot)
    res in
  vres, s


let eval_funexp genv name fexp =
  match fexp.f_kind with
  | Efun ->
      Comb (fun args -> run_comb genv (name, fexp) args)
  | Enode ->
      Seq (
        (* TODO args et res *)
        Stuple [Stuple (List.map (fun _ -> Sempty) fexp.f_args) ;
                Stuple (List.map (fun _ -> Sempty) fexp.f_res) ;
                ieq genv fexp.f_body],
        fun s args -> match s with
        | Stuple [Stuple sf_args; Stuple sf_res; s_body] ->
          run_seq genv (name, fexp) args s_body
        | Stuple s_body ->
    (*       let s = Stuple [Stuple (List.map (fun _ -> Sempty) fexp.f_args) ; Stuple (List.map (fun _ -> Sempty) fexp.f_res) ; Stuple s_body]*)
           run_seq genv (name, fexp) args (Stuple s_body)
      )

let eval_impl genv { desc } = match desc with
  | Eletdecl _ -> failwith "Not implemented."
  | Eletfundecl (name, fexp) -> name, eval_funexp genv name fexp

let genv_init =
  List.fold_left
    (fun acc (k, v) -> Genv.add k (Comb v) acc)
    Genv.empty
    [
      (Name "sleep"), (fun [v1] -> let r = match v1 with
      | Value (Vint i1) -> Unix.sleep i1; Value (Vvoid)
      in [r]);
      (Name "+"), (fun [v1; v2] -> let r = match (v1, v2) with
      | Value (Vint i1), Value (Vint i2) -> Value (Vint (i1 + i2))
      | Vbot, _ -> Vbot
      | Vnil, _ -> Vnil
      | _, Vnil -> Vnil
      | _, Vbot -> Vbot
      in [r]) ;
      (Name "not"), (fun [v1] -> let r = match v1 with
      | Value (Vbool b1) -> Value (Vbool (not b1))
      | Vnil -> Vnil
      | Vbot -> Vbot
      in [r]) ;
      (Name "-"), (fun [v1; v2] -> let r = match (v1, v2) with
        | Value (Vint i1), Value (Vint i2) -> Value (Vint (i1-i2))
        | Vbot, _ -> Vbot
        | _, Vbot -> Vbot
      in [r]) ;
      (Name "*"), (fun [v1; v2] -> let r = match (v1, v2) with
        | Value (Vint i1), Value (Vint i2) -> Value (Vint (i1*i2))
        | Vbot, _ -> Vbot
        | _, Vbot -> Vbot
      in [r]) ;
      (Name "="), (fun [v1; v2] -> let r = match (v1, v2) with
      | Value v1, Value v2 -> Value (Vbool (v1 = v2))
      | Vbot, _ -> Vbot
      | _, Vbot -> Vbot
      in [r]) ;
      (Name "random_bool"),
      (fun [] -> [Value (Vbool (Random.bool ()))])
   ]

let eval_p p =
  List.fold_left
    (fun genv funexp ->
       match funexp.desc with
       | Eletfundecl (name, fexp) ->
           let k, v = eval_impl genv funexp in
           Genv.add (Name k) v genv
       | Eletdecl _ -> failwith "not implemented")
    genv_init
    p

let run_comb f n args =
  let rec runrec i =
    if i = n then i
    else
      let res, _ = f args, S.empty in
      List.iter
        (fun v -> Pprinter.pp_value_ext v; Format.eprintf ", ") res;
      Format.eprintf "\n";
      runrec (i+1) in
  runrec 0

let run_seq f n args init_s =
  let rec runrec s i =
    if i = n then i
    else
      let res, s = f s args in
      List.iter
        (fun v -> Pprinter.pp_value_ext v; Format.eprintf ", ") res;
      Format.eprintf "\n";
      runrec s (i+1) in
  runrec init_s 0


let rec runfind genv name args n =
  if n = 0 then ()
  else
  let f = try
    Genv.find (Name name) genv
  with | Not_found -> failwith "Not found in genv"
  in
  let k = match f with
  | Comb f -> run_comb f n args
  | Seq (s, f) -> run_seq f n args s
  in
  Format.eprintf "\n"


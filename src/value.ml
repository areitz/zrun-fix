(* *********************************************************************)
(*                                                                     *)
(*                        The ZRun Interpreter                         *)
(*                                                                     *)
(*                             Marc Pouzet                             *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique. All rights reserved. This file is distributed under   *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(* Set of values *)
(* noinitialized and non causal values *)

type 'a extended =
  | Vnil : 'a extended
  | Vbot : 'a extended
  | Value : 'a -> 'a extended
  
type pvalue =
  | Vint : int -> pvalue
  | Vbool : bool -> pvalue
  | Vfloat : float -> pvalue
  | Vchar : char -> pvalue
  | Vstring : string -> pvalue
  | Vvoid : pvalue
  | Vtuple : value list -> pvalue
  | Vconstr0 : Lident.t -> pvalue
  | Vconstr1 : Lident.t * value list -> pvalue
  | Vstate0 : Ident.t -> pvalue
  | Vstate1 : Ident.t * value list -> pvalue

and value = pvalue extended
          
type state =
  | Sempty : state
  | Stuple : state list -> state
  | Sval : value -> state
  | Sopt : value option -> state
  | Slazy : state Lazy.t -> state

type ('a, 's) costream =
  | CoF : { init : 's;
            step : 's -> ('a * 's) option } -> ('a, 's) costream

type ('a, 'b, 's) node =
  | CoFun  : ('a -> 'b option) -> ('a, 'b, 's) node
  | CoNode : { init : 's;
               step : 's -> 'a -> ('b * 's) option } -> ('a, 'b, 's) node

type gvalue =
  | Gvalue : value -> gvalue
  | Gfun : (value list, value list, state) node -> gvalue

(* an input entry in the environment *)
open Ast
type 'a ientry = { cur: 'a; default : 'a default }

and 'a default =
  | Val : 'a default
  | Last : 'a -> 'a default
  | Default : 'a -> 'a default
  | Ldefault : exp * state -> 'a default
  | Llast : exp * state -> 'a default

module P3 = struct
  type property = value ientry
  type t = property

  let vbottom = { cur = Vbot; default = Val }
  let bottom = vbottom
  let is_maximal { cur } = match cur with
    | Vbot -> false
    | _ -> true
  let equalc = (=)
  let equald = (=)
  let equal v1 v2 =
    equalc v1.cur v2.cur && equald v1.default v2.default
end

module P4 = struct
  type property = value
  type t = property

  let vbottom = Vbot
  let bottom = vbottom
  let is_maximal v = not (v = Vbot)
  let equal v1 v2 = v1 = v2
end

open Ident
module S = Set.Make(Ident)
module P5 = struct
  type property = value Env.t * S.t
  type t = property
  let bottom = Env.empty, S.empty
  let is_maximal (env, dom) =
    (Env.cardinal env >= 1) && (S.cardinal dom >= 1) &&
    Env.for_all (fun id v -> S.mem id dom && not (v = Vbot)) env
  let equal (env1, dom1) (env2, dom2) =
    S.equal dom1 dom2 &&
    (let dom = dom1 in
    let env1 = Env.fold
      (fun k v m ->
        if S.mem k dom
        then Env.add k v m
        else m) env1 Env.empty in
    let env2 = Env.fold
      (fun k v m ->
        if S.mem k dom
        then Env.add k v m
        else m) env2 Env.empty in
    Env.equal (P4.equal) env1 env2)
end

module P6 = struct
  type property = value Env.t * S.t * state
  type t = property
  let bottom = Env.empty, S.empty, Sempty
  let is_maximal (env, dom, s) =
    (Env.cardinal env >= 1) && (S.cardinal dom >= 1) &&
    Env.for_all (fun id v -> S.mem id dom && not (v = Vbot)) env
  let equal (env1, dom1, s1) (env2, dom2, s2) =
    S.equal dom1 dom2
    &&
    (let dom = dom1 in
    let env1 = Env.fold
      (fun k v m ->
        if S.mem k dom
        then Env.add k v m
        else m) env1 Env.empty in
    let env2 = Env.fold
      (fun k v m ->
        if S.mem k dom
        then Env.add k v m
        else m) env2 Env.empty in
    Env.equal (P4.equal) env1 env2)
    && s1 = s2
end

module P7 = struct
  type property = value ientry Env.t * S.t * state
  type t = property
  let bottom = Env.empty, S.empty, Sempty
  let is_maximal (env, dom, s) =
    (*(Env.cardinal env >= 1) && (S.cardinal dom >= 1) &&
    Env.for_all (fun id v -> S.mem id dom && not (v.cur = Vbot)) env &&*)
    false
  let equal (env1, dom1, s1) (env2, dom2, s2) =
    S.equal dom1 dom2
    &&
    (let dom = dom1 in
    let env1 = Env.fold
      (fun k v m ->
        if S.mem k dom
        then Env.add k v m
        else m) env1 Env.empty in
    let env2 = Env.fold
      (fun k v m ->
        if S.mem k dom
        then Env.add k v m
        else m) env2 Env.empty in
    Env.equal (P4.equal) env1 env2)
    && s1 = s2
end


open Ast
open Graph
open Ident
open Pprinter

open Misc

module Label = struct
  type t = Ident.t
end
module G = Persistent.Digraph.Abstract(Label)

let extract_pateq pateq = pateq.desc

let duplicate b length l =
  if b
  then
    let () = assert (List.length l = 1) in
    List.init length (fun _ -> List.hd l)
  else
    l

let bind_pateq_vvs = List.combine

let dependencies_to_vertices =
  List.map
    (fun (lhs, rhs_set) ->
      let rhs_list = S.elements rhs_set in
      (G.V.create lhs, List.map G.V.create rhs_list)
    )

let add_vertices_of_dependencies g dependencies =
  List.fold_left
    (fun g (lhs, rhs_list) ->
      let vertices_to_add = lhs::rhs_list in
      List.fold_left G.add_vertex g vertices_to_add
    )
    g dependencies

let add_edges_of_dependencies g dependencies =
  List.fold_left
    (fun g (lhs, rhs_list) ->
      List.fold_left
        (fun g v2 -> G.add_edge g lhs v2)
        g rhs_list
    )
    g dependencies


module M = Map.Make(Ident)

let convert_to_list exp = match exp.e_desc with
  | Etuple l -> l
  | _ -> [exp]

let rec extract_leaf exp = match exp.e_desc with
  | Elet(_, _, e) -> extract_leaf e
  | _ -> exp

let rec mkg_equation (g, m) { eq_desc } =
  let g, m = match eq_desc with
  | EQempty ->
      g, m
  | EQlocal(_, eq) ->
      mkg_equation (g, m) eq
  | EQand l ->
      List.fold_left mkg_equation (g, m) l
  | EQeq (p, exp) ->
      (* print_graph g; *)
      let g, m, rhs_vars, flags = mkg_expression (g, m) exp in
      let lhs_vars = extract_pateq p in

      let lhs_length = List.length lhs_vars in
      let rhs_length = List.length rhs_vars in

      let rhs_duplication = flags in
      let rhs_vars = duplicate rhs_duplication lhs_length rhs_vars in

      if !set_verbose then
        Printf.eprintf ">> %s %s\n" (string_of_int (List.length lhs_vars)) (string_of_int (List.length rhs_vars));

      let dependencies_i = bind_pateq_vvs lhs_vars rhs_vars in
      let dependencies_v = dependencies_to_vertices dependencies_i in
      let g = add_vertices_of_dependencies g dependencies_v in
      let g = add_edges_of_dependencies g dependencies_v in

      (* let ... in flattening *)
      let exp = extract_leaf exp in
      (* tuples flattening *)
      let rhs_as_list = match exp.e_desc with
      | Etuple el -> el
      | _ -> [exp]
      in
      if !set_verbose then
        begin
        Printf.eprintf "%s %s : " (string_of_int (List.length lhs_vars)) (string_of_int (List.length rhs_as_list));
        List.iter (fun s -> Printf.eprintf "%s, " (string_of s)) lhs_vars;
        Printf.eprintf " = "; Pprinter.pp_exp exp;
        Printf.eprintf "\n";
        end;
      let m =
        if rhs_duplication
        then List.fold_left (fun m k -> M.add k exp m) m lhs_vars
        else List.fold_left2 (fun m k v -> M.add k v m) m lhs_vars rhs_as_list in
      g, m

  in
  g, m

and mkg_expression (g, m) { e_desc } =
  match e_desc with
  | Econst _ -> g, m, [S.empty], false
  | Elocal id -> g, m, [S.add id S.empty], false
  | Eop(_, e_list) | Eapp(_, e_list) ->
      let g, m, vvs = List.fold_left
        (fun (g, m, vvs) e ->
          let g', m', vvs', _ = mkg_expression (g, m) e in
          let () = assert (List.length vvs' = 1) in
          g', m', S.union vvs (List.hd vvs')
        )
        (g, m, S.empty) e_list
      in g, m, [vvs],
      begin match e_desc with
      | Eapp (s, _) ->
          begin match (Lident.modname s) with
          | "+" -> true
          | "*" -> true
          | _ -> false
          end
      | _ -> false
      end
  | Etuple e_list ->
      let g, m, vvsl = List.fold_left
        (fun (g, m, vvsl) e ->
          let g', m', vvs, _ = mkg_expression (g, m) e in
          let () = assert (List.length vvs = 1) in
          g', m', (List.hd vvs)::vvsl
        )
        (g, m, []) e_list
      in g, m, List.rev vvsl, false
  | Elet(_, eq, e) ->
      let g, m = mkg_equation (g, m) eq in
      let g, m, vvs, b = mkg_expression (g, m) e in g, m, vvs, b

let mkg_funexp name { f_body } =
  mkg_equation (G.empty, M.empty) f_body

let mkg_implementation { desc } =
  match desc with
  | Eletfundecl(f, fd) ->
      f, (mkg_funexp f fd, List.map (fun ve -> ve.var_name) fd.f_args,
                           List.map (fun ve -> ve.var_name) fd.f_res)

let mkg_program i_list =
  List.map mkg_implementation i_list

let print_graph source_name (g, m)  =
  let s = "digraph test {\n" in
  let s = G.fold_vertex
    (fun v s ->
      s ^ (string_of (G.V.label v)) ^ ";\n"
    )
    g s in
  let s = G.fold_edges_e
    (fun e s ->
      let src = string_of (G.V.label (G.E.src e)) in
      let dst = string_of (G.V.label (G.E.dst e)) in
      s ^ src ^ " -> " ^ dst ^ "\n")
    g s in
  let s = s ^ "}\n" in

  (* Format.eprintf "%s\n" s; *)
  let temp_dir = "temp" in
  let basefile = source_name in
  let basename_txt = basefile ^ ".txt" in
  let basename_pdf = basefile ^ ".pdf" in
  let filename_txt = Filename.concat temp_dir basename_txt in
  let filename_pdf = Filename.concat temp_dir basename_pdf in
  let _ = Sys.command ("mkdir -p " ^ temp_dir) in
  let oc = open_out filename_txt in
  Printf.fprintf oc "%s" s;
  close_out oc;
  let _ = Sys.command (
    "dot -Tpdf " ^ filename_txt ^ " -o " ^ filename_pdf) in ();
  (*
  let _ = Sys.command (
    "zathura " ^ filename_pdf) in ();
  *)
  ()

let print_graphs source_name gms = List.iter (print_graph source_name) gms

let execute genv env =
  if !set_verbose then
    Printf.eprintf "Env cardinal: %s.\n" (string_of_int (M.cardinal env));
  (* M.iter (fun k v -> Printf.eprintf "%s ---> " (string_of k); Pprinter.pp_exp v; Printf.eprintf "\n") m; *)
  let valuation = Lfp2.Make.globeval genv env in
  M.iter (fun k v ->
    let r = valuation k in
    Printf.eprintf "%s ---> " (string_of k);
    if Option.is_some r
    then Pprinter.pp_exp (Option.get r)
    else Printf.eprintf "BOT"
    ;
    Printf.eprintf " = ";
    Pprinter.pp_exp v;
    Printf.eprintf "\n"
  ) env;

open New_value.P2
let ppv = function
  | Vbot -> Printf.eprintf "BOT"
  | Vint x -> Printf.eprintf "%s" (string_of_int x)

let execute2 genv env =
  if !set_verbose then
    Printf.eprintf "Env cardinal: %s.\n" (string_of_int (M.cardinal env));
  (* M.iter (fun k v -> Printf.eprintf "%s ---> " (string_of k); Pprinter.pp_exp v; Printf.eprintf "\n") m; *)
  let valuation = Lfp.Make.globeval genv env in
  M.iter (fun k v ->
    let r = valuation k in
    Printf.eprintf "%s ---> " (string_of k);
    ppv r;
    Printf.eprintf " = ";
    Pprinter.pp_exp v;
    Printf.eprintf "\n"
  ) env;

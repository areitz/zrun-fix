open Ident
open Ident2
open Cowrite3
open Ast
open Value
open Misc
open Initial
open Fix

let prints s =
  if !set_verbose then
  Env.iter
    (fun k v ->
       Format.eprintf "%s :" (string_of k);
       S.iter (fun s -> Format.eprintf " %s ;" (string_of s)) v;
       Format.eprintf "\n")
     s

let printv k v =
  if !set_verbose then begin
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v;
  Format.eprintf "\n";
  end

let printenv msg env =
  if !set_verbose then begin
  Format.eprintf "%s\n" msg;
  Env.iter (fun k v ->
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v)
  env;
  Format.eprintf "\n" end

let print s =
  if !set_verbose then Format.eprintf "%s" s

let printvv k v =
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v;
  Format.eprintf "\n"

let vardec_to_ident { var_name } = var_name

let value v = match v with
| Eint i -> Vint i
| Ebool b -> Vbool b
| Efloat f -> Vfloat f
| Evoid -> Vvoid
| Estring s -> Vstring s
| Echar c -> Vchar c


module Make = struct
  module M = Glue.PersistentMapsToImperativeMaps(Env2)
    let i = ref 0
    let globeval cowritem init_env genv body =
      let module F2 = Fix.Make(M)(P5) in
      (*if !i > 10 then failwith "STOP";*)
      F2.lfp
        (fun main_eq valuation ->
  (*Pprinter.pp_equation main_eq;
  Format.eprintf "\n";*)
let rec eval_exp env { e_desc } : value * value Env.t =
  let r = match e_desc with
  | Econst (v) -> Value (value v), env
  | Elocal x ->
     incr i;
     (*for j = 0 to !i-1; do Format.eprintf "  "; done;
     Format.eprintf "<<< %s\n"(string_of x);*)
     let v = if Env.mem x init_env
     then Env.find x init_env
     else
       let eql = Env.find x cowritem in
       let env = List.fold_left
         (fun acc_env eq ->
           let env, _ = valuation eq in
           Env.union (fun k v1 v2 -> Some v2) acc_env env)
         env eql in
       (*Format.eprintf "--> %s : " (string_of x);
       Pprinter.pp_equation e; Format.eprintf "\n";*)
       (*let env, _ = valuation e in*)
       if Env.mem x env
       then Env.find x env
       else begin Format.eprintf "%s\n" (string_of x); Vbot end
     in
     (*for j = 0 to !i-1; do Format.eprintf "  "; done;
     Format.eprintf ">>> %s : " (string_of x);
     decr i;
     Pprinter.pp_value_ext v;
     Format.eprintf "\n";*)
     v, env
  | Eop (Eifthenelse, [e1; e2; e3]) ->
      let v, env = eval_exp env e1 in
      let (Value (Vbool b1)) = v in
      (*if b1
      then eval_exp e2
      else eval_exp e3*)
      let v1, env1 = eval_exp env e2 in
      let v2, env2 = eval_exp env e3 in
      if b1 then v1, env1 else v2, env2
  | Eapp (f, e_list) ->
      let v_list, env = List.fold_left
        (fun (acc, env) e ->
          let v, env = eval_exp env e in v::acc, env)
        ([], env)
        e_list in
      let v_list = List.rev v_list in
      let fv =
        try Genv.find f genv
        with
        | Not_found -> failwith "Not found in genv" in
      let res = fv v_list in
      if List.length res = 1
      then List.hd res, env
      else Value (Vtuple res), env
  (* TODO : take rec into account *)
  | Elet (_, eq, e) ->
      (*Pprinter.pp_equation eq; Format.eprintf "\n";
      Pprinter.pp_exp e; Format.eprintf "\n";
      printenv "#1" env;*)
      let env = eval_eq env eq in
      (*printenv "#2" env;*)
      let v, env = eval_exp env e in
      (*printenv "#3" env;*)
      v, env
  | Eget (i, e) ->
      let v, env = eval_exp env e in
      let Value (Vtuple v_list) = v in
      List.nth v_list i, env
  | Etuple e_list ->
      let v_list, env = List.fold_left
        (fun (acc, env) e ->
          let v, env = eval_exp env e in v::acc, env)
        ([], env)
        e_list in
      let v_list = List.rev v_list in
      Value (Vtuple v_list), env
  in r
and eval_eq env { eq_desc } : value Env.t =
  let r = match eq_desc with
  | EQeq (p, e) ->
      (*incr i;
      for j = 0 to !i-1; do Format.eprintf "  "; done;
      Format.eprintf "<<< %s\n"(string_of id);*)

      let v, env = eval_exp env e in
      if List.length p.desc = 1
      then
        let id = List.hd p.desc in
        Env.singleton id v(*, S.singleton id*)
      else
        (* to be replaced by coiteration matching_pateq *)
        let Value (Vtuple vl) = v in
        let env = List.fold_left2
          (fun acc k v -> Env.add k v acc)
          env
          p.desc
          vl in
        (*let dom = List.fold_left
          (fun acc k -> S.add k acc)
          S.empty
          p.desc in*)
        env(*, dom*)
  | EQlocal (_, eq) -> eval_eq env eq
  | EQempty -> env(*, S.empty*)
  | EQand eql ->
      List.fold_left
        (fun acc_env eq ->
           let env = eval_eq env eq in
           (* on respecte l'ordre du programm *)
           Env.union (fun k v1 v2 -> Some v2) acc_env env)
           (*S.union dom acc_dom*)
        env
        eql
  | EQif (eb, eq1, eq2) ->
      let vb, env = eval_exp env eb in
      let env1 = eval_eq env eq1 in
      let env2 = eval_eq env eq2 in
      let Value (Vbool b) = vb in
      if b then env1 else env2
  | EQassert e ->
      let e, env = eval_exp env e in
      let Value (Vbool b) = e in
      if b then env else failwith "assertion failed"

      (*for j = 0 to !i-1; do Format.eprintf "  "; done;
      Format.eprintf ">>> %s : " (string_of id);
      decr i;
      Pprinter.pp_value_ext v;
      Format.eprintf "\n";*)
  | _ -> failwith "not implemented"
  in r
in
eval_eq Env.empty main_eq, main_eq.eq_write)
end

let run genv fv args =
  let name, fexp = fv
  in
  let init_env = List.fold_left2
    (fun acc k v -> Env.add k v acc)
    Env.empty
    (List.map vardec_to_ident fexp.f_args)
    args
  in
  let cowritem = cowrite fexp.f_body in

  let res = List.map vardec_to_ident fexp.f_res in
  let module M = Make in
  let solution = M.globeval cowritem init_env genv fexp.f_body in
  let env, _ = solution fexp.f_body in
  let vres = List.map
    (fun id ->
    try Env.find id env with | Not_found -> Vbot)
    res in
  vres


let eval_funexp genv name fexp =
  (fun args -> run genv (name, fexp) args)

let eval_impl genv { desc } = match desc with
  | Eletdecl _ -> failwith "Not implemented."
  | Eletfundecl (name, fexp) -> name, eval_funexp genv name fexp

let genv_init =
  List.fold_left
    (fun acc (k, v) -> Genv.add k v acc)
    Genv.empty
    [
      (Name "+"), (fun [v1 ; v2] -> let r = match (v1, v2) with
      | Value (Vint i1), Value (Vint i2) -> Value (Vint (i1 + i2))
      | Vbot, _ -> Vbot
      | _, Vbot -> Vbot in
      [r]) ;
      (Name "not"), (fun [v1] -> let r = match v1 with
      | Value (Vbool b1) -> Value (Vbool (not b1))
      | Vbot -> Vbot
      in [r]) ;
      (Name "-"), (fun [v1; v2] -> let r = match (v1, v2) with
        | Value (Vint i1), Value (Vint i2) -> Value (Vint (i1-i2))
        | Vbot, _ -> Vbot
        | _, Vbot -> Vbot
      in [r]) ;
      (Name "*"), (fun [v1; v2] -> let r = match (v1, v2) with
        | Value (Vint i1), Value (Vint i2) -> Value (Vint (i1*i2))
        | Vbot, _ -> Vbot
        | _, Vbot -> Vbot
      in [r]) ;
      (Name "="), (fun [v1; v2] -> let r = match (v1, v2) with
      | Value v1, Value v2 -> Value (Vbool (v1 = v2))
      | Vbot, _ -> Vbot
      | _, Vbot -> Vbot
      in [r]) ;
      (Name "random_bool"),
      (fun [] -> [Value (Vbool (Random.bool ()))])
   ]

let eval_p p =
  List.fold_left
    (fun genv funexp ->
       match funexp.desc with
       | Eletfundecl (name, fexp) ->
           let k, v = eval_impl genv funexp in
           Genv.add (Name k) v genv
       | Eletdecl _ -> failwith "not implemented")
    genv_init
    p

let runfind genv name args =
  let f = try
    Genv.find (Name name) genv
  with | Not_found -> failwith "Not found in genv"
  in
  List.iter (fun v -> Pprinter.pp_value_ext v; Format.eprintf ", ") (f args);
  Format.eprintf "\n"


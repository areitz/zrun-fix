open Ident
open Cowrite2
open Ast
open Value
open Dflow
open Misc
open Initial
open Fix
open Ident
open Common

let prints s =
  if !set_verbose then
  Env.iter
    (fun k v ->
       Format.eprintf "%s :" (string_of k);
       S.iter (fun s -> Format.eprintf " %s ;" (string_of s)) v;
       Format.eprintf "\n")
     s

let printv k v =
  if !set_verbose then begin
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v;
  Format.eprintf "\n";
  end

let print s =
  if !set_verbose then Format.eprintf "%s" s

let printvv k v =
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v;
  Format.eprintf "\n"

let vardec_to_ident { var_name } = var_name

let value v = match v with
| Eint i -> Vint i
| Ebool b -> Vbool b
| Efloat f -> Vfloat f
| Evoid -> Vvoid
| Estring s -> Vstring s
| Echar c -> Vchar c

module Make = struct
  module M = Glue.PersistentMapsToImperativeMaps(Env)
    let i = ref 0
    let globeval cowritem init_env genv =
      let module F2 = Fix.Make(M)(P5) in
      (*printm2 cowritem2;*)
      (*let env = ref Env.empty in
      let getenv () = env in*)
      F2.lfp
        (fun id valuation ->


let rec eval_exp genv valuation { e_desc } : P4.t =
  let r = match e_desc with
  | Econst (v) -> Value (value v)
  | Elocal x ->
     incr i;
     (*for j = 0 to !i-1; do Format.eprintf "  "; done;
     Format.eprintf "<<< %s\n"(string_of x);*)
     let v = if Env.mem x init_env
     then Env.find x init_env
     else
       let env, _ = valuation x in
       if Env.mem x env
       then Env.find x env
       else begin Format.eprintf "!!%s" (string_of id); Vbot end
     in
     (*for j = 0 to !i-1; do Format.eprintf "  "; done;
     Format.eprintf ">>> %s : " (string_of x);
     decr i;
     Pprinter.pp_value_ext v;
     Format.eprintf "\n";*)
     v
  | Eop (Eifthenelse, [e1; e2; e3]) ->
      let v1 = eval_exp genv valuation e1 in
      let (Value (Vbool b1)) = v1 in
      (*if b1
      then eval_exp genv valuation e2
      else eval_exp genv valuation e3*)
      let v2 = eval_exp genv valuation e2 in
      let v3 = eval_exp genv valuation e3 in
      if b1 then v2 else v3
  | Eapp (f, e_list) ->
      let v_list = List.rev @@ List.fold_left
        (fun acc e -> let v = eval_exp genv valuation e in v::acc)
        []
        e_list in
      let fv = try Genv.find f genv with | Not_found -> failwith "Not found in genv"  in
      let res = fv v_list in
      if List.length res = 1
      then List.hd res
      else Value (Vtuple res)
  | Elet (_, _, e) -> let v = eval_exp genv valuation e in v
  | Eget (i, e) ->
      let v = eval_exp genv valuation e in
      let Value (Vtuple v_list) = v in
      List.nth v_list i
  | Etuple e_list ->
      let v_list = List.map (eval_exp genv valuation) e_list in
      Value (Vtuple v_list)
  in r
and eval_eq genv valuation { eq_desc } : P5.t =
  let r = match eq_desc with
  | EQeq (p, e) ->
      assert (List.length p.desc = 1);
      let id = List.hd p.desc in
      incr i;
      (*for j = 0 to !i-1; do Format.eprintf "  "; done;
      Format.eprintf "<<< %s\n"(string_of id);*)

      let v = eval_exp genv valuation e in
      (*for j = 0 to !i-1; do Format.eprintf "  "; done;
      Format.eprintf ">>> %s : " (string_of id);
      decr i;
      Pprinter.pp_value_ext v;
      Format.eprintf "\n";*)
      Env.singleton id v, S.singleton id
  | _ -> failwith "not implemented"
  in r
in
let eq =
  try Env.find id cowritem
  with | Not_found ->
    Format.eprintf "!%s!!\n" (string_of id);
    printm cowritem;
    failwith "not foundn in cowrite" in
let s = eval_eq genv valuation eq in
s)
end

let run genv fv args =
  let name, cowrite_m2, eql, fexp = fv 
  in
  let init_env = List.fold_left2
    (fun acc k v -> Env.add k v acc)
    Env.empty
    (List.map vardec_to_ident fexp.f_args)
    args
  in

  let res = List.map vardec_to_ident fexp.f_res in
  (*Format.eprintf "---%s---\n" name;*)
  printm cowrite_m2;
  let module M = Make in
  let solution = M.globeval cowrite_m2 init_env genv in
  let vres = List.map
    (fun id -> let env, _ = solution id in
    (*Format.eprintf "---> %i\n" (Env.cardinal env);*)
    try Env.find id env with | Not_found -> Vbot)
      (*Format.eprintf "-> %s\n" (string_of id);
      let sol = solution id in
      sol)*)
    res in
  (*let v_out = List.map
    (fun id ->
      Format.eprintf "-> %s\n" (string_of id);
      let sol = solution id in
      sol)*)
  vres


let eval_funexp genv name fexp =
  let cowrite_m = cowrite_eq fexp.f_body in

  print "Cowrite 1\n";
  printm cowrite_m;
  let cowrite_m2 =
    Env.fold
      (fun k v m -> let e =  match v.eq_desc with
        | EQeq (p, e) -> e
        | _ -> failwith "should be an eqeq"
        in
        Env.add k e m)
      cowrite_m
      Env.empty in
  print "Cowrite 2\n";
  printm2 cowrite_m2;
  let eql = Env.fold
    (fun id v eql -> v::eql)
    cowrite_m
    []
  in
  let args = List.fold_left
    (fun s id -> S.add id s)
    S.empty
    (List.map vardec_to_ident fexp.f_args)
  in
  (* List.iter
    (fun eq -> Pprinter.pp_equation eq; Format.eprintf "\n") eql;*)
  (fun args -> run genv (name, cowrite_m, eql, fexp) args)

let eval_impl genv { desc } = match desc with
  | Eletdecl _ -> failwith "Not implemented."
  | Eletfundecl (name, fexp) -> name, eval_funexp genv name fexp

let genv_init =
  List.fold_left
    (fun acc (k, v) -> Genv.add k v acc)
    Genv.empty
    [
      (Name "+"), (fun [v1 ; v2] ->
      let Value (Vint i1) = v1 in
      let Value (Vint i2) = v2 in
      [Value (Vint (i1+i2))]) ;
      (*(Name "-"), (fun [Value (Vint i1) ; Value (Vint i2) ]
      -> [Value (Vint (i1 - i2))]) ;*)

      (Name "-"),
      (fun [v1; v2] -> let r = match (v1, v2) with
        | Value (Vint i1), Value (Vint i2) -> Value (Vint (i1-i2))
        | Vbot, _ -> Vbot
        | _, Vbot -> Vbot
      in [r]) ;
      (Name "*"),
      (fun [v1; v2] -> let r = match (v1, v2) with
        | Value (Vint i1), Value (Vint i2) -> Value (Vint (i1*i2))
        | Vbot, _ -> Vbot
        | _, Vbot -> Vbot
      in [r]) ;
      (*(fun [Value (Vint i1); Value (Vint i2)]
      -> [Value (Vint (i1*i2))]) ;*)
      (Name "="), (fun [Value v1; Value v2] -> [Value (Vbool (v1 = v2))]) ;

      (Name "random_bool"),
      (fun [] -> [Value (Vbool (Random.bool ()))])
   ]

let eval_p p =
  List.fold_left
    (fun genv funexp ->
       match funexp.desc with
       | Eletfundecl (name, fexp) ->
           let k, v = eval_impl genv funexp in
           Genv.add (Name k) v genv
       | Eletdecl _ -> failwith "not implemented")
    genv_init
    p

let runfind genv name args =
  let f = try
    Genv.find (Name name) genv
  with | Not_found -> failwith "Not found in genv"
  in
  List.iter (fun v -> Pprinter.pp_value_ext v; Format.eprintf ", ") (f args);
  Format.eprintf "\n"


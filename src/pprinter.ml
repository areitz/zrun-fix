open Ast
open Ident
open Lident
open Value

let pp_const c = match c with
  | Eint i -> Format.eprintf "%s" (string_of_int i)
  | Ebool b -> Format.eprintf "%s" (string_of_bool b)
  | Efloat f -> Format.eprintf "%s" (string_of_float f)

let pp_pateq p =
  List.iter (fun id -> Format.eprintf "%s, " (string_of id)) p.desc


let rec pp_equation { eq_desc } =
  match eq_desc with
  | EQempty ->
      Format.eprintf "EQEMPTY\n";
      ()
  | EQlocal(vars, eq) ->
      Format.eprintf "ELOCAL : ";
      Format.eprintf "local [...]\ndo\n";
      pp_equation eq;
      Format.eprintf "done\n"
  | EQand(eq_list) ->
      Format.eprintf "EQAND :\n";
      List.iter
        (fun eq ->
          Format.eprintf "and ";
          pp_equation eq;
          Format.eprintf "\n"
        )
        eq_list
  | EQeq (p, exp) ->
      Format.eprintf "EQEQ : ";
      (* Format.eprintf "[...] "; *)
      pp_pateq p;
      Format.eprintf " = ";
      pp_exp exp
  | EQif (e, eq1, eq2) ->
      Format.eprintf "EQIF : (";
      pp_exp e;
      Format.eprintf ", ";
      pp_equation eq1;
      Format.eprintf ", ";
      pp_equation eq2;
      Format.eprintf ")"
  | EQmatch (e, _) ->
      Format.eprintf "EQMATCH : ";
      pp_exp e
  | EQautomaton (b, _) ->
      Format.eprintf "EQAUTOMATON : ";
      Format.eprintf "%s" (string_of_bool b)
  | EQreset (eq, e) ->
      Format.eprintf "RESET ";
      pp_equation eq;
      Format.eprintf " EVERY ";
      pp_exp e
  | EQassert (e) ->
      Format.eprintf "EQASSERT : ";
      pp_exp e


and pp_exp { e_desc } =
  match e_desc with
  | Econst c ->
      (* Format.eprintf "CONST "; *)
      pp_const c
  | Elocal id ->
      Format.eprintf "LOCAL ";
      Format.eprintf "%s" (string_of id)
  | Elast id ->
      Format.eprintf "LAST ";
      Format.eprintf "%s" (string_of id)
  | Eglobal _ -> Format.eprintf "GLOBAL "
  | Econstr0 f -> Format.eprintf "CONSTR0 "; Format.eprintf "%s" (modname f)
  | Eop _ ->
      Format.eprintf "OP"
  | Etuple e_list ->
      Format.eprintf "TUPLE ";
      Format.eprintf "(";
      List.iter
        (fun exp ->
          pp_exp exp;
          Format.eprintf ", "
        )
        e_list;
      Format.eprintf ")"
  | Elet(b, eq, e) ->
      Format.eprintf "LET ";
      if b then Format.eprintf "REC ";
      pp_equation eq;
      Format.eprintf " IN ";
      pp_exp e
  | Eapp(f, e_list) ->
      Format.eprintf "APP ";
      Format.eprintf "%s(" (modname f);
      List.iter
        (fun exp ->
          pp_exp exp;
          Format.eprintf ", "
        )
        e_list;
      Format.eprintf ")"
  | Eget (i, e) ->
      Format.eprintf "GET %i " i;
      pp_exp e
  | _ -> Format.eprintf  "Unknown.\n"

let pp_funexp { f_body } = pp_equation f_body

let pp_implementation { desc } =
  match desc with
  | Eletdecl(f, e) ->
      Format.eprintf "ELETDECL : ";
      Format.eprintf "let %s\n" f;
      pp_exp e
  | Eletfundecl(f, fd) ->
      Format.eprintf "EFUNDECL : ";
      Format.eprintf "let %s\n" f;
      pp_funexp fd

open Value
let rec pp_value_ext v = match v with
  | Vnil -> Format.eprintf "NIL";
  | Vbot -> Format.eprintf "BOT";
  | Value v -> pp_value v

and pp_value v = match v with
  | Vint i -> Format.eprintf "INT %s" (string_of_int i)
  | Vbool b -> Format.eprintf "BOOL %s" (string_of_bool b)
  | Vfloat f -> Format.eprintf "FLOAT %s" (string_of_float f) 
  | Vchar c -> Format.eprintf "CHAR %s" (String.make 1 c)
  | Vstring s -> Format.eprintf "STRING %s" s
  | Vvoid -> Format.eprintf "VOID"
  | Vtuple vl -> List.iter (fun v -> pp_value_ext v; Format.eprintf ", ") vl
  | Vconstr0 f -> Format.eprintf "CONSTR0 %s" (modname f)

and pp_state s = match s with
  | Sempty -> Format.eprintf "SEMPTY\n"
  | Stuple sl -> Format.eprintf "STUPLE :\n";
      List.iter (fun s -> Format.eprintf "and ";
                          pp_state s;
                          Format.eprintf "\n") sl
  | Sval v -> Format.eprintf "SVAL : ";
              pp_value_ext v;
              Format.eprintf "\n";
  | Sopt v -> Format.eprintf "SOPT : ";
              if Option.is_some v
              then begin Format.eprintf "SOME "; pp_value_ext (Option.get v) end
              else Format.eprintf "NONE";
              Format.eprintf "\n"
  | Slazy _ -> Format.eprintf "SLAZY\n"

let pp_program i_list = List.iter pp_implementation i_list

let rec pp_state s = begin match s with
  | Sempty -> Format.eprintf "SEMPTY"
  | Sval v -> Format.eprintf "SVALUE"; pp_value_ext v
  | Stuple vl -> Format.eprintf "STUPLE (";
                List.iter (fun v -> pp_state v) vl;
                Format.eprintf ")"
  | Slazy _ -> Format.eprintf "SLAZY"
  | Sopt _ -> Format.eprintf "OPT"
  end;
  Format.eprintf "\n"

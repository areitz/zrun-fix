open Ast
open Ident

(* TODO ajouter à eqlocal les variables intermédiaires crées *)

let rec normalize_eq eq =
  let desc = match eq.eq_desc with
  | EQempty -> EQempty
  | EQlocal (evl, eq) -> EQlocal (evl, normalize_eq eq)
  | EQand eql -> EQand (List.map normalize_eq eql)
  | EQeq (p, e) ->
      let e = normalize_exp e in
      if List.length p.desc = 1
      then EQeq (p, e)
      else
        let ids = p.desc in
        let main_id = fresh "M" in
        let new_p = { p with desc = [main_id] } in
        let main_eq_desc = EQeq (new_p, e) in
        let aux_eqs_descs = List.mapi
          (fun k id ->
          let aux_eq_p = { p with desc = [id] } in
          let main_as_exp = { e with e_desc = Elocal main_id } in
          let aux_eq_e = { e with e_desc = Eget (k, main_as_exp) } in
          EQeq (aux_eq_p, aux_eq_e))
          ids in
        let eqs_descs = main_eq_desc::aux_eqs_descs in
        let eqs = List.map
          (fun desc -> { eq_desc = desc;
                         eq_write = S.empty;
                         eq_loc = eq.eq_loc })
          eqs_descs in
        EQand eqs
  in
  { eq with eq_desc = desc }

and normalize_exp e =
  let desc = match e.e_desc with
  | Econst _ | Elocal _ -> e.e_desc
  | Eop (op, el) -> Eop (op, List.map normalize_exp el)
  | Etuple el -> Etuple (List.map normalize_exp el)
  | Eapp (lid, el) -> Eapp (lid, List.map normalize_exp el)
  | Eget (i, e) -> Eget (i, normalize_exp e)
  | Elet (b, eq, e) -> Elet (b, normalize_eq eq, normalize_exp e)
  in
  { e with e_desc = desc }

let normalize_funexp fexp =
  let body = normalize_eq fexp.f_body in
  { fexp with f_body = body }

let normalize_impl impl =
  let desc = match impl.desc with
  | Eletdecl (n, e) -> Eletdecl (n, normalize_exp e)
  | Eletfundecl (n, fexp) -> Eletfundecl (n, normalize_funexp fexp)
  in
  { impl with desc = desc }

let normalize_p = List.map normalize_impl


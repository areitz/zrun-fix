open Ast
open Ident
open Location

let rec print x = match x with
  | [] -> ()
  | [id] -> Format.eprintf "%s.\n" (string_of id)
  | h::t -> Format.eprintf "%s, " (string_of h); print t

module M' = struct
  type t = eq
  (* essentiel : https://github.com/ocaml/ocaml/blob/trunk/stdlib/map.ml#L135 *)
  (* hackish, to be improved *)
  let compare eq1 eq2 = match (eq1.eq_loc, eq2.eq_loc) with
    | Loc (i1, j1), Loc (i2, j2) when i1 = j1 && i2 = j2 ->
        Int.compare i1 i2
    | Loc (i1, j1), Loc (i2, j2) ->
        Pprinter.pp_equation eq1; Format.eprintf  "\n";
        Pprinter.pp_equation eq2; Format.eprintf  "\n";
        Format.eprintf "%i %i %i %i\n" i1 j1 i2 j2;
        failwith "write loc error"
end

module Env2 = Map.Make(M')

open Value
type node =
  | Comb of (value list -> value list)
  | Seq of (state * (state -> value list -> value list * state))

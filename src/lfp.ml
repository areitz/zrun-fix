open Ast
open Fix

open Lident

module M = Map.Make(Ident)

module Error = struct
  type error =
    | MissingArgs
    | TooFewArguments
    | TooManyArguments
    | ExpectedType
    | NestedTuples
  exception Err of error
  let error kind = raise (Err kind)
end

open New_value
module Make = struct
  module V = struct
    type t = Ident.t
  end
  module F =
    Fix.ForType(V)(P2)
  open P2

  let int_of_pvalue = function
  | Vint x -> x
  | _ -> Error.error ExpectedType

  let bool_of_pvalue = function
  | Vbool b -> b
  | _ -> Error.error ExpectedType

  let float_of_pvalue = function
  | Vfloat f -> f
  | _ -> Error.error ExpectedType

  let char_of_pvalue = function
  | Vchar c -> c
  | _ -> Error.error ExpectedType

  let string_of_pvalue = function
  | Vstring s -> s
  | _ -> Error.error ExpectedType

  let is_bottom x = (x = Vbot)

  let value_to_exp v =
    let desc = match v with
    | Vvoid -> Econst (Evoid)
    | Vint x -> Econst (Eint x)
    | Vbool b -> Econst (Ebool b)
    | Vfloat f -> Econst (Efloat f)
    in
    { e_desc = desc; e_loc = Location.no_location }

  let find env id = 
    try
      M.find id env
    with | Not_found -> Error.error MissingArgs

  let rec eval genv exp valuation =
    let exp_to_eval = exp.e_desc in
    let exp_loc = exp.e_loc in

    match exp_to_eval with
    | Econst c ->
        begin match c with
        | Eint x -> Vint x
        | Ebool b -> Vbool b
        | Efloat f -> Vfloat f
        end
    | Elocal id -> valuation id
    | Eapp (op, el) ->
        let el = List.map (fun exp -> eval genv exp valuation) el in
        if List.exists is_bottom el
        then Vbot
        else
          begin match (modname op) with
          | "+" -> let [a;b] = List.map int_of_pvalue el in Vint (a + b)
          | "*" -> let [a;b] = List.map int_of_pvalue el in Vint (a * b)
          | s -> call s genv el exp_loc
          end
    (* tuples have been previously "flattened" in dot.ml,
    manually *)
    | Etuple _ -> Error.error NestedTuples
    (* let intermediate equations have also been previously "flattened" in dot.ml,
    using the map m ident -> exp *)
    | Elet (_, _, exp) ->
        eval genv exp valuation
  and globeval (genv : ((string * (exp M.t * Ident.t list * Ident.t list)) list)) (env : exp M.t)  =
    F.lfp (fun id valuation -> eval genv (find env id) valuation)

  and call s genv actual_args exp_loc =
    let ext_env, formal_args, ext_res  = List.assoc s genv in
    let actual_args = List.map value_to_exp actual_args in
    let ext_env  = List.fold_left2 (fun m k v -> M.add k v m) ext_env formal_args actual_args in
    let ext_valuation = globeval genv ext_env in
    match (List.length ext_res) with
    | 0 -> Vbot
    | 1 -> ext_valuation (List.hd ext_res)
    | n -> let res = List.map ext_valuation ext_res in
           if List.exists is_bottom  res
           then Vbot
           else
             Vtuple res
end


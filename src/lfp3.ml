open Fix

open Ident
open Monad.Opt
open Value
open Common

module Make = struct
  module V = struct
    type t = Ident.t
  end
  module F =
    Fix.ForType(V)(Prop.Option(P3))

  let globeval dom sem s_eq env =
    F.lfp
      (fun id valuation ->
        let env2 = List.fold_left
          (fun env id ->
             let v = valuation id in
             if Option.is_some v
             then Env.add id (Option.get v) env
             else env
          )
          env dom
        in
        let s = sem s_eq env2 in
        if Option.is_some s
        then
          return (Env.find id (fst (Option.get s)))
        else
          return (Env.find id env)
      )
end


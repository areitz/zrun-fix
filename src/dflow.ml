open Fix
open Ident
open Value
open Ast
open Initial

module type CONTAINER = sig
  val cowrite_map : exp Env.t
  val successors : Ident.t -> Ident.t list
  val eval : P4.t Env.t -> exp -> P4.t
  val roots : Ident.t list
  val init_env : P4.t Env.t
end

module Make
  (C : CONTAINER)
= struct
  module M = Glue.PersistentMapsToImperativeMaps(Env)
  module P = struct
    include Env
    type property = P4.t Env.t
    let leq_join p q =
      Env.union
        (* /!\ *)
        (fun id v1 v2 -> Some v2) p q
  end
  module DFG = struct
    type variable = M.key
    type property = P.property

    let cowrite_map = C.cowrite_map
    let successors = C.successors
    let eval = C.eval
    let cowrite x = Env.find x cowrite_map
    let roots = C.roots
    let init_env = C.init_env

    let print_ienv env =
      Format.eprintf "IENV\n";
      Env.iter (fun k v ->
        Format.eprintf "-> %s : " (string_of k);
        Pprinter.pp_value_ext v;
        Format.eprintf "\n") env

    let foreach_root contribute =
      List.iter
        (fun id ->
           if Env.mem id init_env
           then
             begin
             (*Format.eprintf "eval11: %s\n" (string_of id);*)
             contribute id init_env
             end
           else
             begin
             (*Format.eprintf "eval12: %s\n" (string_of id);*)
             let env = init_env in
             let e = cowrite id in
             let v = eval env e in
             let env = Env.add id v env in
             (*print_ienv env;*)
             contribute id env
             end)
        roots


    let foreach_successor id env contribute =
      (*Format.eprintf "%s : " (string_of id);
      List.iter (fun v -> Format.eprintf "%s ;" (string_of v)) (successors id);
      Format.eprintf "\n";*)
      List.iter
        (fun id' ->
           if Env.mem id' init_env
           then
             begin
             (*Format.eprintf "eval21: %s\n" (string_of id');*)
             contribute id' env
             end
           else
             begin
             (*Format.eprintf "eval22: %s\n" (string_of id');*)
             if Env.mem id' env
             then contribute id' env
             else
               begin
               let e = cowrite id' in
               let v = eval env e in
               (*Pprinter.pp_value_ext v; Format.eprintf "\n";*)
               let env' = Env.add id' v env in
               (*print_ienv env';*)
               contribute id' env'
               end
           end)
        (successors id)
  end
  let solution =
    let module F = DataFlow.Run(M)(P)(DFG) in
    F.solution
end

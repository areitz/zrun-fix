open Ident
open Cowrite2
open Ast
open Value
open Dflow
open Misc
open Initial

let add_order id k m =
  (* ce qui nous intéresse c'est la première apparition *)
  if not (Env.mem id m)
  then Env.add k id m, k+1
  else m, k

let collect_deps exp =
  let rec collect_deps_aux acc k exp = match exp.e_desc with
  | Econst _ -> acc, k
  | Elocal id -> add_order id k acc
  | Eop (_, el) | Etuple el | Eapp (_, el) ->
      List.fold_left
        (fun (acc, k) e -> collect_deps_aux acc k e)
        (acc, k)
        el
  | Elet (_, _, e) -> collect_deps_aux acc k e
  | Eget (_, e) -> collect_deps_aux acc k e
  in
  collect_deps_aux Env.empty 0 exp  

let successors_eq eq (acc_succs, acc_roots) = match eq.eq_desc with
  | EQeq (p, e) ->
      let p = p.desc in
      (* wlog thanks to normalization *)
      assert (List.length p = 1);
      let u = List.hd p in
      (* add u as successors to any s \in deps is not enough:
      we need deps sorted from rightmost to leftmost:
      d1, d2, ..., dk
      we want the chain: d1 -> d2 -> ... -> dk -> u
      *)  
      let deps, k = collect_deps e in
      Format.eprintf "##%s : %i\n" (string_of u) (Env.cardinal deps);
      let acc_succs' =
        Env.fold
        (fun acc (d, i) ->
          let u = d in
          let v = if i < k then Env.find (i+1) deps

S.fold
        (fun v acc ->
           let succs_v = if Env.mem v acc
             then Env.find v acc
             else S.empty
           in
           Env.add v (S.add u succs_v) acc)
        deps
        acc_succs
      in
      let acc_roots' = if S.cardinal deps = 0 then u::acc_roots else acc_roots
      in
      acc_succs', acc_roots'
  | _ -> failwith "should be an eqeq"

let successors eql =
  List.fold_left
    (fun acc eq -> successors_eq eq acc)
    (Env.empty, [])
    eql

let prints s =
  if !set_verbose then
  Env.iter
    (fun k v ->
       Format.eprintf "%s :" (string_of k);
       S.iter (fun s -> Format.eprintf " %s ;" (string_of s)) v;
       Format.eprintf "\n")
     s

let printv k v =
  if !set_verbose then begin
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v;
  Format.eprintf "\n";
  end

let print s =
  if !set_verbose then Format.eprintf "%s" s

let printvv k v =
  Format.eprintf "%s : " (string_of k);
  Pprinter.pp_value_ext v;
  Format.eprintf "\n"

let vardec_to_ident { var_name } = var_name

let value v = match v with
| Eint i -> Vint i
| Ebool b -> Vbool b
| Efloat f -> Vfloat f
| Evoid -> Vvoid
| Estring s -> Vstring s
| Echar c -> Vchar c

let rec eval_exp genv (env : P4.t Env.t) { e_desc } : P4.t =
  let r = match e_desc with
  | Econst (v) -> Value (value v)
  | Elocal x ->
     if Env.mem x env
     then Env.find x env
     else begin Format.eprintf "#### %s\n" (string_of x); failwith "notpartofenv" end
  | Eop (Eifthenelse, [e1; e2; e3]) ->
      let v1 = eval_exp genv env e1 in
      let v2 = eval_exp genv env e2 in
      let v3 = eval_exp genv env e3 in
      let (Value (Vbool b1)) = v1 in
      if b1 then v2 else v3
  | Eapp (f, e_list) ->
      let v_list = List.rev @@ List.fold_left
        (fun acc e -> let v = eval_exp genv env e in v::acc)
        []
        e_list in
      let fv = Genv.find f genv in
      let res = fv v_list in
      if List.length res = 1
      then List.hd res 
      else Value (Vtuple res)
  | Elet (_, _, e) -> let v = eval_exp genv env e in v
  | Eget (i, e) ->
      let v = eval_exp genv env e in
      let Value (Vtuple v_list) = v in
      List.nth v_list i
  | Etuple e_list ->
      let v_list = List.map (eval_exp genv env) e_list in
      Value (Vtuple v_list)
  in r

let run genv fv args =
  let cowrite_m2, succs, roots, eql, fexp = fv in
  let init_env = List.fold_left2
    (fun acc k v -> Env.add k v acc)
    Env.empty
    (List.map vardec_to_ident fexp.f_args)
    args
  in
  let module C = struct
    let cowrite_map = cowrite_m2
    let successors x =
      if Env.mem x succs
      then let v = Env.find x succs in S.elements v
      else []
    let eval = eval_exp genv
    let roots = roots
    let init_env = init_env
  end
  in
  let res = List.map vardec_to_ident fexp.f_res in
  let module M = Dflow.Make(C) in
  let solution = M.solution in
  let vres = List.map
    (fun id -> let sol = solution id in
               if Option.is_some sol
               then
                 let sol = Option.get sol in
                 let v = if Env.mem id sol then Env.find id sol else failwith "notpartofsol" in
                 v
               else failwith "notsol")
    res in
  List.iter2 (fun k v -> printvv k v) res vres;
  vres

let eval_funexp genv fexp =
  let cowrite_m = cowrite_eq fexp.f_body in

  print "Cowrite 1\n";
  printm cowrite_m;
  let cowrite_m2 =
    Env.fold
      (fun k v m -> let e =  match v.eq_desc with
        | EQeq (p, e) -> e
        | _ -> failwith "should be an eqeq"
        in
        Env.add k e m)
      cowrite_m
      Env.empty in
  print "Cowrite 2\n";
  printm2 cowrite_m2;
  let eql = Env.fold
    (fun id v eql -> v::eql)
    cowrite_m
    []
  in
  (* List.iter
    (fun eq -> Pprinter.pp_equation eq; Format.eprintf "\n") eql;*)
  let succs, roots = successors eql in
  print "Successors\n";
  prints succs;
  (*(fun genv -> run genv ()*)
  (*cowrite_m2, succs, eql, fexp*)
  (fun args -> run genv (cowrite_m2, succs, roots, eql, fexp) args)

let eval_impl genv { desc } = match desc with
  | Eletdecl _ -> failwith "Not implemented."
  | Eletfundecl (name, fexp) -> name, eval_funexp genv fexp

let genv_init =
  List.fold_left
    (fun acc (k, v) -> Genv.add k v acc)
    Genv.empty
    [
      (Name "+"), (fun [v1 ; v2] ->
      let Value (Vint i1) = v1 in
      let Value (Vint i2) = v2 in
      [Value (Vint (i1+i2))]) ;
      (Name "-"), (fun [Value (Vint i1) ; Value (Vint i2) ]
      -> [Value (Vint (i1 - i2))]) ;

      (Name "*"),
      (fun [Value (Vint i1); Value (Vint i2)]
      -> [Value (Vint (i1*i2))]) ;
      (Name "="), (fun [Value v1; Value v2] -> [Value (Vbool (v1 = v2))]) ;

      (Name "random_bool"),
      (fun [] -> [Value (Vbool (Random.bool ()))])
   ]

let eval_p p =
  List.fold_left
    (fun genv funexp ->
       let k, v = eval_impl genv funexp in
       Genv.add (Name k) v genv)
    genv_init
    p

let runfind genv name args =
  let f = Genv.find (Name name) genv in
  f args

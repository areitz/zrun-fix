(* *********************************************************************)
(*                                                                     *)
(*                        The ZRun Interpreter                         *)
(*                                                                     *)
(*                             Marc Pouzet                             *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique. All rights reserved. This file is distributed under   *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(* A Denotational Semantics for Zelus *)
(* This is a companion file of working notes on the co-iterative *)
(* semantics presented at the SYNCHRON workshop, December 2019, *)
(* the class on "Advanced Functional Programming" given at Bamberg
   Univ. in June-July 2019 and the Master MPRI - M2, Fall 2019 *)
open Misc
open Monad
open Opt                                                            
open Ident
open Ast
open Value
open Initial
open Debug

let find_value_opt x env =
  let* { cur } = Env.find_opt x env in
  return cur

let find_last_opt x env =
  let* { default } = Env.find_opt x env in
  match default with
  | Last(v) -> return v
  | _ -> None
           
let find_gnode_opt x env =
  let* v = Genv.find_opt x env in
  match v with
  | Gfun(f) -> return f
  | _ -> None

let find_gvalue_opt x env =
  let* v = Genv.find_opt x env in
  match v with
  | Gvalue(v) -> return v
  | _ -> None

let names env = Env.fold (fun n _ acc -> S.add n acc) env S.empty

(* value of an immediate constant *)
let value v =
  match v with
  | Eint(v) -> Vint(v)
  | Ebool(b) -> Vbool(b)
  | Evoid -> Vvoid
  | Efloat(f) -> Vfloat(f)
  | Echar(c) -> Vchar(c)
  | Estring(s) -> Vstring(s)

(* evaluation functions *)

(* the bottom environment *)
let bot_env eq_write =
  S.fold (fun x acc -> Env.add x { cur = Vbot; default = Val } acc)
    eq_write Env.empty

(* the nil environment *)
let nil_env eq_write =
  S.fold (fun x acc -> Env.add x { cur = Vnil; default = Val } acc)
    eq_write Env.empty

let size { eq_write } = S.cardinal eq_write

(* a bot/nil value lifted to lists *)
let bot_list l = List.map (fun _ -> Vbot) l
let nil_list l = List.map (fun _ -> Vnil) l

let eptf =
  if !set_verbose
  then Format.eprintf
  else (fun x -> ())

(* merge two environments provided they do not overlap *)
let merge env1 env2 =
  let s = Env.to_seq env1 in
  seqfold
    (fun acc (x, entry) ->
      if Env.mem x env2 then None
      else return (Env.add x entry acc))
    env2 s

(* let by env env_handler write now requires sexp *)
      
(* complete [env_handler] with inputs from [write] *)
(* pre-condition: [Dom(env_handler) subseteq write] *)
let complete env env_handler write =
  S.fold
    (fun x acc ->
      match Env.find_opt x env_handler with
      | Some(entry) -> Env.add x entry acc
      | None ->
         match Env.find_opt x env with
         | Some(entry) -> Env.add x entry acc
         | None -> acc (* this case should not arrive *))
    write Env.empty
  
(* given [env] and [env_handler = [x1 \ { cur1 },..., xn \ { curn }] *)
(* returns [x1 \ { cu1; default x env },..., xn \ { curn; default x env }] *)
let complete_with_default env env_handler =
  Env.fold
    (fun x ({ cur } as entry) acc ->
      match Env.find_opt x env with
      | None -> Env.add x entry acc
      | Some { default } -> Env.add x { entry with default = default } acc)
    env_handler Env.empty

(* equality of values in the fixpoint iteration. Because of monotonicity *)
(* only compare bot/nil with non bot/nil values. *)
let equal_values v1 v2 =
  match v1, v2 with
  | (Vbot, Vbot) | (Vnil, Vnil) | (Value _ , Value _) -> true
  | _ -> false

(* Dynamic check of causality. A set of equations is causal when all defined *)
(* variables are non bottom, provided free variables are non bottom *)
(* this a very strong constraint. In particular, it rejects the situation *)
(* of a variable that is bottom but not used. *)
(* causal(env)(env_out)(names) =
 *-               /\ (forall x in Dom(env), env(x) <> bot)
 *-               /\ (env_out, _ = fixpoint_eq genv sem eq n s_eq bot)
 *-               => (forall x in names /\ Dom(env_out), env_out(x) <> bot) *)
let causal env env_out names =
  let bot v = match v with | Vbot -> true | _ -> false in
  let bot_name n =
    let r = find_value_opt n env_out in
    match r with | None -> false | Some(v) -> bot v in
  let bot_names =
    if Env.for_all (fun _ { cur } -> not (bot cur)) env
    then S.filter bot_name names else S.empty in
  let pnames ff names = S.iter (Ident.fprint_t ff) names in
  if not !set_nocausality then
    if S.is_empty bot_names then ()
    else
      begin
        Format.eprintf "The following variables are not causal:\n\
                        %a@." pnames bot_names;
        raise Stdlib.Exit
      end

(*
(* bounded fixpoint combinator *)
(* computes a pre fixpoint f^n(bot) <= fix(f) *)
let fixpoint n stop f s bot =
  let rec fixpoint n v =
    if n <= 0 then (* this case should not happen *)
      return (0, v, s)
    else
      (* compute a fixpoint for the value [v] keeping the current state *)
      let* v', s' = f s v in
      if stop v v' then return (n, v', s') else fixpoint (n-1) v' in
  (* computes the next state *)
  fixpoint n bot

(* stop the fixpoint when two successive environments are equal *)
let equal_env env1 env2 =
  Env.equal
    (fun { cur = cur1} { cur = cur2 } -> (equal_values cur1 cur2))
    env1 env2

(* bounded fixpoint for a set of equations *)
let fixpoint_eq genv env sem eq n s_eq bot =
  let sem s_eq env_in =
    let env = Env.append env_in env in
    let* env_out, s_eq = sem genv env eq s_eq in
    let env_out = complete_with_default env env_out in
    return (env_out, s_eq) in
  print_number "Max number of iterations:" n;
  print_ienv "Fixpoint. Inital env is:" bot;
  let* m, env_out, s_eq = fixpoint n equal_env sem s_eq bot in
  print_ienv "Fixpoint. Result env is:" env_out;
  print_number "Actual number of iterations:" (n - m);
  (* sum := !sum + (n - m + 1);
  max := !max + n; *)
  print_number "Max was:" n;
  print_ienv "End of fixpoint with env:" env_out;
  (* incr_number_of_fixpoint_iterations (n - m + 1);*)
  return (env_out, s_eq)
*)


(***************************)
(* default (comb) then init and last (seq) *)
let sdvardec env { var_name; var_default } s v =
  let* default = match var_default, s with
  (* standard cases *)
  | Ewith_nothing, Sempty ->
      return Val
  | Ewith_last, Sval ve ->
      return (Last ve)
  (* modified cases *)
  | Ewith_default e, se ->
      (* fst (sexp _ _ e se) yet to be computed *)
      return (Ldefault (e, se))
  | Ewith_init e, Stuple [si; se] ->
      let* lv = match si with
      | Sopt None ->
          (* first instant *)
          (* fst (sexp _ _ e se) yet to be computed *)
          return (Llast (e, se))
      | Sval v | Sopt (Some v) ->
          return (Last v)
      | _ -> None
      in
      return lv
  | _ -> None
  in
  let r = Env.add var_name { cur = v; default = default } env in
  return r

let rec sdexp env { e_desc } s  =
  match e_desc, s with
  | (Econst _ | Econstr0 _ | Elocal _ | Eglobal _ | Elast _), _ ->
      return env
  | Eget (_, e), s -> sdexp env e s
  | Elet (_, eq, e), Stuple [s_eq; s] ->
      let* env = sdeq env eq s_eq in
      let* env = sdexp env e s in
      return env
  | Etuple el, Stuple sl | Eapp (_, el), Stuple (_::sl) ->
      Opt.fold2 sdexp env el sl
  | Eop(Efby, [_; e2]), Stuple [_; s2] ->
      sdexp env e2 s2
  | Eop(Efby, [e1; e2]), Stuple [_; s1; s2] ->
      let* env = sdexp env e1 s1 in
      let* env = sdexp env e2 s2 in
      return env
  | Eop(Eunarypre, [e]), Stuple [_; s] ->
      sdexp env e s
  | Eop(Eminusgreater, [e1; e2]), Stuple [_; s1; s2] ->
      let* env = sdexp env e1 s1 in
      let* env = sdexp env e2 s2 in
      return env
  | Eop(Eifthenelse, [e1; e2; e3]), Stuple [s1; s2; s3] ->
      let* env = sdexp env e1 s1 in
      let* env = sdexp env e2 s2 in
      let* env = sdexp env e3 s3 in
      return env
  | Econstr1 (_, el), Stuple sl ->
      Opt.fold2 sdexp env el sl

and sdeq env { eq_desc } s =
  match eq_desc, s with
  | EQeq (_, e), s -> sdexp env e s
  | EQif (e, eq1, eq2), Stuple[se; seq1; seq2] ->
      let* env = sdexp env e se in
      let* env = sdeq env eq1 seq1 in
      let* env = sdeq env eq2 seq2 in
      return env
  | EQand eql, Stuple sl ->
      Opt.fold2 sdeq env eql sl
  | EQreset (eq, e), Stuple [seq; se] ->
      let* env = sdeq env eq seq in
      let* env = sdexp env e se in
      return env
  | EQempty, s -> return env
  | EQassert e, s -> sdexp env e s
  | EQlocal (vdl, eq), Stuple [Stuple sl; s_eq] -> (* /!\ *)
      sdblock env vdl eq sl s_eq
  | EQmatch _, _ -> (* TODO *) return env
  | EQautomaton _, _ -> (* TODO *) return env

(*and sdmatch env *)
and sdblock env vdl eq sl s_eq =
  let* env =
    Opt.fold3
      sdvardec env vdl sl (bot_list vdl) in
  sdeq env eq s_eq

(* [sem genv env e = CoF f s] such that [iexp genv env e = s] *)
(* and [sexp genv env e = f] *)
(* initial state *)
(* env is useless *)
let rec iexp genv env { e_desc; e_loc } =
  let r = match e_desc with
  | Econst _ | Econstr0 _ | Elocal _ | Eglobal _ | Elast _ ->
     return Sempty
  | Econstr1(_, e_list) ->
     let* s_list = Opt.map (iexp genv env) e_list in
     return (Stuple(s_list))
  | Eop(op, e_list) ->
     begin match op, e_list with
     | Efby, [{ e_desc = Econst(v) }; e] ->
        (* synchronous register initialized with a static value *)
        let* s = iexp genv env e  in
        return (Stuple [Sval(Value (value v)); s])
     | Efby, [e1; e2] ->
        let* s1 = iexp genv env e1 in
        let* s2 = iexp genv env e2 in
        return (Stuple [Sopt(None); s1; s2])
     | Eunarypre, [e] -> 
        (* un-initialized synchronous register *)
        let* s = iexp genv env e in
        return (Stuple [Sval(Vnil); s])
     | Eminusgreater, [e1; e2] ->
        let* s1 = iexp genv env e1 in
        let* s2 = iexp genv env e2 in
        return (Stuple [Sval(Value(Vbool(true))); s1; s2])
     | Eifthenelse, [e1; e2; e3] ->
          let* s1 = iexp genv env e1 in
          let* s2 = iexp genv env e2 in
          let* s3 = iexp genv env e3 in
          return (Stuple [s1; s2; s3])
     | _ -> None
     end
  | Etuple(e_list) -> 
     let* s_list = Opt.map (iexp genv env) e_list in
     return (Stuple(s_list))
  | Eget(_, e) ->
     let* s = iexp genv env e in
     return s
  | Eapp(f, e_list) ->
     let* s_list = Opt.map (iexp genv env) e_list in
     let* v = find_gnode_opt f genv in
     let s =
       match v with | CoFun _ -> Sempty | CoNode { init = s } -> s in
     return (Stuple(s :: s_list))
  | Elet(is_rec, eq, e) ->
     let* s_eq = ieq genv env eq in
     let* se = iexp genv env e in
     return (Stuple [s_eq; se]) in
  stop_at_location e_loc r 1
    
and ieq genv env { eq_desc; eq_loc } =
  let r = match eq_desc with
  | EQeq(_, e) -> iexp genv env e
  | EQif(e, eq1, eq2) ->
     let* se = iexp genv env e in
     let* seq1 = ieq genv env eq1 in
     let* seq2 = ieq genv env eq2 in
     return (Stuple [se; seq1; seq2])
  | EQand(eq_list) ->
     let* seq_list = Opt.map (ieq genv env) eq_list in
     return (Stuple seq_list)
  | EQlocal(v_list, eq) ->
     (*ieq genv env eq*)
     let* s_v_list = Opt.map (ivardec genv env) v_list in
     let* s_eq = ieq genv env eq in
     return (Stuple [Stuple s_v_list; s_eq])
  | EQreset(eq, e) ->
     let* s_eq = ieq genv env eq in
     let* se = iexp genv env e in
     return (Stuple [s_eq; se])
  | EQautomaton(_, a_h_list) ->
     let* s_list = Opt.map (iautomaton_handler genv env) a_h_list in
     (* The initial state is the first in the list *)
     (* it is not reset at the first instant *)
     let* a_h = List.nth_opt a_h_list 0 in
     let* i = initial_state_of_automaton a_h in
     return (Stuple(Sval(Value(i)) :: Sval(Value(Vbool(false))) :: s_list))
  | EQmatch(e, m_h_list) ->
     let* se = iexp genv env e in
     let* sm_list = Opt.map (imatch_handler genv env) m_h_list in
     return (Stuple (se :: sm_list))
  | EQempty -> return Sempty
  | EQassert(e) ->
     let* se = iexp genv env e in
     return se
  in
  stop_at_location eq_loc r 2

and ivardec genv env { var_default; var_loc } =
  let r = match var_default with
  | Ewith_init(e) ->
     let* s = iexp genv env e in return (Stuple [Sopt(None); s])
  | Ewith_default(e) -> iexp genv env e
  | Ewith_nothing -> return Sempty
  | Ewith_last -> return (Sval(Vnil)) in
  stop_at_location var_loc r 3

and imatch_handler genv env { m_vars; m_body } =
  let* sv_list = Opt.map (ivardec genv env) m_vars in
  let* sb = ieq genv env m_body in
  return (Stuple[Stuple(sv_list); sb])

and iautomaton_handler genv env { s_vars; s_body; s_trans } =
  let* sv_list = Opt.map (ivardec genv env) s_vars in
  let* sb = ieq genv env s_body in
  let* st_list = Opt.map (iescape genv env) s_trans in
  return (Stuple [Stuple(sv_list); sb; Stuple(st_list)])

(* initial state of an automaton *)
and initial_state_of_automaton { s_state = { desc } } =
  match desc with
  | Estate0pat(f) -> return (Vstate0(f))
  | _ -> None

and iescape genv env { e_cond; e_vars; e_body; e_next_state } =
  let* s_cond = iscondpat genv env e_cond in
  let* sv_list = Opt.map (ivardec genv env) e_vars in
  let* s_body = ieq genv env e_body in
  let* s_state = istate genv env e_next_state in
  return (Stuple [s_cond; Stuple(sv_list); s_body; s_state])

and iscondpat genv env e_cond = iexp genv env e_cond

and istate genv env { desc } =
  match desc with
  | Estate0 _ -> return Sempty
  | Estate1(_, e_list) ->
     let* s_list = Opt.map (iexp genv env) e_list in
     return (Stuple(s_list))

(* pattern matching *)
(* match the value [v] against [x1,...,xn] *)
let rec matching_pateq { desc } v =
  match desc, v with
  | [x], _ -> return (Env.singleton x { cur = v; default = Val })
  | x_list, Vbot -> matching_list Env.empty x_list (bot_list x_list)
  | x_list, Vnil -> matching_list Env.empty x_list (nil_list x_list)
  | x_list, Value(Vtuple(v_list)) -> matching_list Env.empty x_list v_list
  | _ -> None

and matching_list env x_list v_list =
    match x_list, v_list with
    | [], [] ->
       return env
    | x :: x_list, v :: v_list ->
       matching_list (Env.add x { cur = v; default = Val } env) x_list v_list
    | _ -> None

(* match a state f(v1,...,vn) against a state name f(x1,...,xn) *)
let matching_state ps { desc } =
  match ps, desc with
  | Vstate0(f), Estate0pat(g) when Ident.compare f g = 0 -> Some Env.empty
  | Vstate1(f, v_list), Estate1pat(g, id_list) when Ident.compare f g = 0 ->
     matching_list Env.empty id_list v_list
  | _ -> None

(* match a value [ve] against a pattern [p] *)
let matching_pattern ve { desc } =
  match ve, desc with
  | Vconstr0(f), Econstr0pat(g) when Lident.compare f g = 0 -> Some(Env.empty)
  | _ -> None
       
(* [reset init step genv env body s r] resets [step genv env body] *)
(* when [r] is true *)
let reset init step genv env body s r =
  let*s = if r then init genv env body else return s in
  step env body s

let set_vardec env_eq { var_name; var_loc } s =
  let r = match s with
    | Sempty -> return Sempty
    | Sopt _ | Sval _ ->
       let* v = find_value_opt var_name env_eq in
       return (Sval(v))
    | Stuple [_; se] ->
       let* v = find_value_opt var_name env_eq in
       return (Stuple [Sval v; se])
    | _ -> None in
  stop_at_location var_loc r 8



(************************************************************************)
open Ident2
open Fix
module Make = struct
  module M = Glue.PersistentMapsToImperativeMaps(Env2)
  let globeval cowritem init_env init_env2 genv body smap init_state1 init_state2 =
    let module F = Fix.Make(M)(P7) in
    let smap = if Option.is_some smap
               then Option.get smap
               else Env2.empty in
    F.lfp
      (fun main_eq valuation ->

(* step function *)
(* the value of an expression [e] in a global environment [genv] and local *)
(* environment [env] is a step function of type [state -> (value * state) option] *)
(* [None] is return in case of type error; [Some(v, s)] otherwise *)
let rec sexp env ({ e_desc = e_desc; e_loc } as exp) s =
  let r = match e_desc, s with   
  | Econst(v), s ->
     return (Value (value v), env, s)
  | Econstr0(f), s ->
     return (Value (Vconstr0(f)), env, s)
  | Elocal x, s ->
     let* v =
       if Env.mem x init_env
       then return (Env.find x init_env)
       else
         begin
         let eql = Env.find x cowritem in
         let env = List.fold_left
           (fun acc_env eq ->
             let env, _, _ = valuation eq in
             Env.union (fun k v1 v2 -> Some v2) acc_env env)
           env eql in
         if Env.mem x env
         then return (Env.find x env)
         else return ({ cur = Vbot; default = Val })
         end
     in
     let v = v.cur in
     return (v, env, s)
  | Eglobal g, s ->
     let* v = find_gvalue_opt g genv in
     return (v, env, s)
  | Elast x, s ->
     let* { default } = Env.find_opt x env in
     begin match default with
     | Last v -> return (v, env, s)
     | Llast (e, se) ->
         let* v', env', _ = sexp env e se in
         return (v', env', s)
     | _ -> None
     end
     (*let* v = find_last_opt x env in
     return (v, env, s)*)
  | Eop(op, e_list), s ->
     begin match op, e_list, s with
     | (* initialized unit-delay with a constant *)
       Efby, [_; e2], Stuple [Sval(v); s2] ->
        let* v2, env2, s2 = sexp env e2 s2  in
        return (v, env2, Stuple [Sval(v2); s2])
     | Efby, [e1; e2], Stuple [Sopt(v_opt); s1; s2] ->
        let* v1, env1, s1 = sexp env e1 s1  in
        let* v2, env2, s2 = sexp env e2 s2  in
        (* is-it the first instant? *)
        let v =
          match v_opt with | None -> v1 | Some(v) -> v in
        return (v, env2, Stuple [Sopt(Some(v2)); s1; s2])
     | Eunarypre, [e], Stuple [Sval(v); s] -> 
        let* ve, env, s = sexp env e s in
        return (v, env, Stuple [Sval(ve); s])
     | Eminusgreater, [e1; e2], Stuple [Sval(v); s1; s2] ->
       (* when [v = true] this is the very first instant. [v] is a reset bit *)
       (* see [paper EMSOFT'06] *)
        let* v1, env, s1 = sexp env e1 s1  in
        let* v2, env, s2 = sexp env e2 s2  in
        let* v_out = ifthenelse v v1 v2 in
        return (v_out, env, Stuple [Sval(Value(Vbool(false))); s1; s2])
     | Eifthenelse, [e1; e2; e3], Stuple [s1; s2; s3] ->
        let* v1, env, s1 = sexp env e1 s1 in
        let* v2, env, s2 = sexp env e2 s2 in
        let* v3, env, s3 = sexp env e3 s3 in
        let* v = ifthenelse v1 v2 v3 in
        return (v, env, Stuple [s1; s2; s3])
     | _ -> None
     end
  (* Econstr1 *)
  | Etuple(e_list), Stuple(s_list) ->
     let* v_list, env, s_list = sexp_list env e_list s_list in
     (* check that all component are not nil nor bot *)
     return (strict_tuple v_list, env, Stuple s_list)
  | Eget(i, e), s ->
     let* v, env, s = sexp env e s in
     let* v = Initial.geti i v in
     return (v, env, s)
  (* s = état de la fonction *)
  | Eapp(f, e_list), Stuple (s :: s_list) ->
     let* v_list, env, s_list = sexp_list env e_list s_list in
     let* fv = find_gnode_opt f genv in
     let* v, s =
       match fv with
       | CoFun(fv) ->
          let* v_list = fv v_list in 
          let* v = tuple v_list in
          return (v, Sempty)
       | CoNode { step = fv } ->
          let* v_list, s = fv s v_list in
          let* v = tuple v_list in
          return (v, s) in
     return (v, env, Stuple (s :: s_list)) 
  | Elet(false, eq, e), Stuple [s_eq; s] ->
     let* env, s_eq = seq env eq s_eq in
     (*let env = Env.append env_eq env in*)
     let* v, env, s = sexp env e s in
     return (v, env, Stuple [s_eq; s])
  | Elet(true, _, e), Stuple [s_eq; s] ->
     (*let env = Env.append env_eq env in*)
     let* v, env, s = sexp env e s in
     return (v, env, Stuple [s_eq; s])
  (*| Elet(true, ({ eq_write } as eq), e), Stuple [s_eq; s] ->
     (* compute a bounded fix-point in [n] steps *)
     let bot = bot_env eq_write in
     let n = size eq in
     let n = if n <= 0 then 0 else n+1 in
     let* env_eq, s_eq =
        if !only_one_fixpoint = 1
        then seq genv env eq s_eq
        else fixpoint_eq genv env seq eq n s_eq bot
     in
     (* a dynamic check of causality: all defined names in [eq] *)
     (* must be non bottom provided that all free vars. are non bottom *)
     let _ = causal env env_eq eq_write in
     let env = Env.append env_eq env in
     let* v, s = sexp genv env e s in
     return (v, Stuple [s_eq; s])*)
  | _ -> None in
  stop_at_location e_loc r 4

(* given an environment [env], a local environment [env_handler] *)
(* an a set of written variables [write] *)
(* complete [env_handler] with entries in [env] for variables that *)
(* appear in [write] *)
(* this is used for giving the semantics of a control-structure, e.g.,: *)
(* [if e then do x = ... and y = ... else do x = ... done]. When [e = false] *)
(* the value of [y] is the default one given at the definition of [y] *)
(* for the moment, we treat the absence of a default value as a type error *)
and by env env_handler write =
  S.fold
    (fun x acc ->
      (* if [x in env] but not [x in env_handler] *)
      (* then either [x = last x] or [x = default(x)] depending *)
      (* on what is declared for [x]. *)
      if Env.mem x env_handler then acc
      else begin
      if Env.mem x env then
      let* { default } as entry = Env.find_opt x env in
        match default with
        | Val -> None
        | Last(v) | Default(v) ->
           let* acc = acc in
           return (Env.add x { entry with cur = v } acc)
        | Llast(e, s) | Ldefault(e,s) ->
           let* v, _, _ = sexp Env.empty e s in
           let* acc = acc in
           return (Env.add x { entry with cur = v } acc)
           else acc end)
    write (return env_handler) 
 

and sexp_list env e_list s_list : (value list * value ientry Env.t * state list) option =
  match e_list, s_list with
  | [], [] ->
     return ([], env, [])
  | e :: e_list, s :: s_list ->
     let* v, env, s = sexp env e s in
     let* v_list, env, s_list = sexp_list env e_list s_list in
     return (v :: v_list, env, s :: s_list)
  | _ -> None


(* step function for an equation *)
and seq env ({ eq_desc; eq_write; eq_loc } as eq) s =
  if !set_verbose then Pprinter.pp_equation eq;
  let r = match eq_desc, s with 
  | EQeq(p, e), s -> 
     let* v, env, s1 = sexp env e s in
     (* /!\ env *)
     let* env_p1 = matching_pateq p v in
     Some (env_p1, s1) (* return (env_p, s))) *)
  | EQif(e, eq1, eq2), Stuple [se; s_eq1; s_eq2] ->
      let* v, env, se = sexp env e se in
      let* env_eq, s =
        match v with
        (* if the condition is bot/nil then all variables have value bot/nil *)
        | Vbot -> return (bot_env eq_write, Stuple [se; s_eq1; s_eq2])
        | Vnil -> return (nil_env eq_write, Stuple [se; s_eq1; s_eq2])
        | Value(b) ->
          let* v = bool b in
          if v then
            let* env1, s_eq1 = seq env eq1 s_eq1 in
            (* complete the output environment with default *)
            (* or last values from all variables defined in [eq_write] but *)
            (* not in [env1] *)
            let* env1 = by env env1 eq_write in
            return (env1, Stuple [se; s_eq1; s_eq2])
          else
            let* env2, s_eq2 = seq env eq2 s_eq2 in
            (* symetric completion *)
            let* env2 = by env env2 eq_write in
            return (env2, Stuple [se; s_eq1; s_eq2]) in
    return (env_eq, s)
  | EQand(eq_list), Stuple(s_list) ->
     let* env_eq, s_list =
        let seq env acc eq s =
           let* env_eq, s = seq env eq s in
           (* /!\ *)
           (*let* acc = merge env_eq acc in*)
           let acc = Env.union
             (fun k v1 v2 -> match v1.cur, v2.cur with
             | _, Vbot -> Some v1
             | _ -> Some v2
             )
             acc env_eq in
           return (acc, s)
         in
         mapfold2 (seq env) Env.empty eq_list s_list
     in
     return (env_eq, Stuple(s_list))
  | EQreset(eq, e), Stuple [s_eq; se] ->
     let* v, env, se = sexp env e se in
     let* env_eq, s_eq =
       match v with
       (* if the condition is bot/nil then all variables have value bot/nil *)
       | Vbot -> return (bot_env eq_write, Stuple [s_eq; se])
       | Vnil -> return (nil_env eq_write, Stuple [s_eq; se])
       | Value(v) ->
          let* v = bool v in
          reset ieq seq genv env eq s_eq v in
     return (env_eq, Stuple [s_eq; se])
  (*| EQlocal(v_list, eq), s_eq -> seq env eq s_eq*)
  | EQlocal(v_list, eq), Stuple [Stuple(s_list); s_eq] ->
     let* _, env, s_list, s_eq = sblock env v_list eq s_list s_eq in
     return (env, Stuple [Stuple(s_list); s_eq])
  | EQautomaton(is_weak, a_h_list), Stuple (Sval(ps) :: Sval(pr) :: s_list) ->
     (* [ps = state where to go; pr = whether the state must be reset or not *)
     let* env, ns, nr, s_list =
       match ps, pr with
       | (Vbot, _) | (_, Vbot) ->
          return (bot_env eq_write, ps, pr, s_list)
       | (Vnil, _) | (_, Vnil) ->
          return (nil_env eq_write, ps, pr, s_list)
       | Value(ps), Value(pr) ->
          let* pr = bool pr in
          sautomaton_handler_list
            is_weak genv env eq_write a_h_list ps pr s_list in
     return (env, Stuple (Sval(ns) :: Sval(nr) :: s_list))
  | EQmatch(e, m_h_list), Stuple (se :: s_list) ->
     let* ve, env, se = sexp env e se in
     let* env, s_list =
       match ve with
       (* if the condition is bot/nil then all variables have value bot/nil *)
       | Vbot -> return (bot_env eq_write, s_list)
       | Vnil -> return (nil_env eq_write, s_list)
       | Value(ve) ->
          smatch_handler_list genv env ve eq_write m_h_list s_list in
     return (env, Stuple (se :: s_list))
  | EQempty, s -> return (Env.empty, s)
  | EQassert(e), s ->
     let* ve, env, s = sexp env e s in
     (* when ve is not bot/nil it must be true *)
     (* for the moment we raise a type error *)
     let* s =
       match ve with
       | Vnil | Vbot -> return s
       | Value(v) ->
          let* v = bool v in
          (* stop when [no_assert = true] *)
          if !no_assert then return s
          else if v then return s else None in
     return (env, s)
  | _ -> None
  in
  stop_at_location eq_loc r 5

(* block [local x1 [init e1 | default e1 | ],..., xn [...] do eq done *)
and sblock env v_list ({ eq_write; eq_loc } as eq) s_list s_eq =
  let* env_v, s_list =
    Opt.mapfold3
      (svardec env) Env.empty v_list s_list (bot_list v_list) in
  (*let bot = complete env env_v eq_write in
  let n = size eq in
  let n = if n <= 0 then 0 else n+1 in
  let* env_eq, s_eq = fixpoint_eq genv env seq eq n s_eq bot in*)
  let env = Env.append env_v env in
  let* env_eq, s_eq = seq env eq s_eq in
  (*let env = Env.append env_eq env in*)
  (* a dynamic check of causality for [x1,...,xn] *)
  let _ = causal env env_eq (names env_v) in
  (* store the next last value *)
  let* s_list = Opt.map2 (set_vardec env_eq) v_list s_list in
  (* remove all local variables from [env_eq] *)
  (* let env = Env.append env_eq env in *)
  (*let env_eq = remove env_v env_eq in*)
  let r = return (env_eq, env_eq, s_list, s_eq) in
  stop_at_location eq_loc r 6

and sblock_with_reset env v_list eq s_list s_eq r =
  let* s_list, s_eq =
    if r then
      (* recompute the initial state *)
      let* s_list = Opt.map (ivardec genv env) v_list in
      let* s_eq = ieq genv env eq in
      return (s_list, s_eq)
    else
      (* keep the current one *)
      return (s_list, s_eq) in
  sblock env v_list eq s_list s_eq

(* rôle de env, rôle de acc ? *)
and svardec env acc { var_name; var_default; var_loc } s v =
  let* default, _, s =
    match var_default, s with
    | Ewith_nothing, Sempty -> (* [local x in ...] *)
       return (Val, acc, s)
    | Ewith_init(e), Stuple [si; se] ->
       let* ve, env', se = sexp env e se in
       let* lv =
         match si with
         | Sopt(None) ->
            (* first instant *)
            return (Last(ve))
         | Sval(v) | Sopt(Some(v)) -> return (Last(v))
         | _ -> None in
       return (lv, env', Stuple [si; se])
    | Ewith_default(e), se ->
       let* ve, env', se = sexp env e se in
       return (Default(ve), env', se)
    | Ewith_last, Sval(ve) -> (* [local last x in ... last x ...] *)
       return (Last(ve), acc, s)
    | _ -> None in
  let r = Env.add var_name { cur = v; default = default } acc in
  let r = return(r, s) in
  stop_at_location var_loc r 7

(*
(* store the next value for [last x] in the state of [vardec] declarations *)
and set_vardec env_eq { var_name; var_loc } s =
*)

(*
(* remove entries [x, entry] from [env_eq] for *)
(* every variable [x] defined by [env_v] *)
and remove env_v env_eq =
  Env.fold (fun x _ acc -> Env.remove x acc) env_v env_eq
*)

and sautomaton_handler_list is_weak genv env eq_write a_h_list ps pr s_list =
  (* automaton with weak transitions *)
  let rec body_and_transition_list a_h_list s_list =
    match a_h_list, s_list with
    | { s_state; s_vars; s_body; s_trans; s_loc } :: a_h_list,
      (Stuple [Stuple(ss_var_list); ss_body;
               Stuple(ss_trans)] as s) :: s_list ->
     let r = matching_state ps s_state in
     let* env_handler, ns, nr, s_list =
       match r with
       | None ->
          (* this is not the good state; try an other one *)
          let* env_handler, ns, nr, s_list =
            body_and_transition_list a_h_list s_list in
          return (env_handler, ns, nr, s :: s_list)
       | Some(env_state) ->
          let env = Env.append env_state env in
          (* execute the body *)
          let* env, env_body, ss_var_list, ss_body =
            sblock_with_reset env s_vars s_body ss_var_list ss_body pr in
          (* execute the transitions *)
          let* env_trans, (ns, nr), ss_trans =
            sescape_list genv env s_trans ss_trans ps pr in
          return
            (env_body, ns, nr,
             Stuple [Stuple(ss_var_list); ss_body;
                     Stuple(ss_trans)] :: s_list) in
     (* complete missing entries in the environment *)
     let* env_handler = by env env_handler eq_write in
     return (env_handler, ns, nr, s_list)
    | _ -> None in

  (* automaton with strong transitions *)
  (* 1/ choose the active state by testing unless transitions of the *)
  (* current state *)
  let rec transition_list a_h_list s_list =
    match a_h_list, s_list with
    | { s_state; s_trans } :: a_h_list,
      (Stuple [ss_var; ss_body; Stuple(ss_trans)] as s) :: s_list ->
     let r = matching_state ps s_state in
     begin match r with
       | None ->
          (* this is not the good state; try an other one *)
          let* env_trans, ns, nr, s_list = transition_list a_h_list s_list in
          return (env_trans, ns, nr, s :: s_list)
       | Some(env_state) ->
          let env = Env.append env_state env in
          (* execute the transitions *)
          let* env_trans, (ns, nr), ss_trans =
            sescape_list genv env s_trans ss_trans ps pr in
          return
            (env_trans, ns, nr,
             Stuple [ss_var; ss_body; Stuple(ss_trans)] :: s_list)
     end
    | _ -> None in


  (* 2/ execute the body of the target state *)
  let rec body_list a_h_list ps pr s_list =
    match a_h_list, s_list with
    | { s_state; s_vars; s_body } :: a_h_list,
      (Stuple [Stuple(ss_var_list); ss_body; ss_trans] as s) :: s_list ->
       let r = matching_state ps s_state in
       begin match r with
         | None ->
            (* this is not the good state; try an other one *)
            let* env_body, s_list = body_list a_h_list ps pr s_list in
            return (env_body, s :: s_list)
         | Some(env_state) ->
            let env = Env.append env_state env in
            (* execute the body *)
            let* _, env_body, ss_var_list, ss_body =
              sblock_with_reset env s_vars s_body ss_var_list ss_body pr in
            return
              (env_body, Stuple [Stuple(ss_var_list); ss_body;
                                 ss_trans] :: s_list)
       end
   | _ -> None in
  if is_weak then
    body_and_transition_list a_h_list s_list
  else
    (* chose the active state *)
    let* env_trans, ns, nr, s_list = transition_list a_h_list s_list in
    (* execute the current state *)
    match ns, nr with
    | (Vbot, _) | (_, Vbot) ->
       return (bot_env eq_write, ns, nr, s_list)
    | (Vnil, _) | (_, Vnil) ->
       return (bot_env eq_write, ns, nr, s_list)
    | Value(vns), Value(vnr) ->
       let* vnr = bool vnr in
       let* env_body, s_list = body_list a_h_list vns vnr s_list in
       let env_handler = Env.append env_trans env_body in
       (* complete missing entries in the environment *)
       let* env_handler = by env env_handler eq_write in
       return (env_handler, ns, nr, s_list)



(* [Given a transition t, a name ps of a state in the automaton, a value pr *)
(* for the reset condition, *)
(* [escape_list genv env { e_cond; e_vars; e_body; e_next_state } ps pr] *)
(* returns an environment, a new state name, a new reset condition and *)
(* a new state *)
and sescape_list genv env escape_list s_list ps pr =
  match escape_list, s_list with
  | [], [] -> return (Env.empty, (Value ps, Value (Vbool false)), [])
  | { e_cond; e_reset; e_vars; e_body; e_next_state; e_loc } :: escape_list,
    Stuple [s_cond; Stuple(s_var_list); s_body; s_next_state] :: s_list ->
      (* if [pr=true] then the transition is reset *)
     let* v, env, s_cond = reset iscondpat sscondpat genv env e_cond s_cond pr in
     let* env_body, (ns, nr), s =
       match v with
       (* if [v = bot/nil] the state and reset bit are also bot/nil *)
       | Vbot ->
          return (bot_env e_body.eq_write,
                  (Vbot, Vbot), Stuple [s_cond; Stuple(s_var_list);
                                        s_body; s_next_state] :: s_list)
       | Vnil ->
          return (nil_env e_body.eq_write,
                  (Vnil, Vnil), Stuple [s_cond; Stuple(s_var_list);
                                        s_body; s_next_state] :: s_list)
       | Value(v) ->
          (* revoir le traitement. L'etat des conditions *)
          (* change mais les equations ne sont evaluees que lorsque *)
          (* la condition est vraie *)
          (* le code ci-dessous ne le fait pas. *)
          let* v = bool v in
          let* env, env_body, s_var_list, s_body =
            sblock_with_reset env e_vars e_body s_var_list s_body pr in
          let* ns, s_next_state = sstate genv env e_next_state s_next_state in
          let* env_others, (s, r), s_list =
            sescape_list genv env escape_list s_list ps pr in
          let ns, nr =
            if v then (ns, Value(Vbool(e_reset))) else (s, r) in
          return (env_body, (ns, nr),
                  Stuple [s_cond; Stuple(s_var_list);
                          s_body; s_next_state] :: s_list) in
     let r = return (env_body, (ns, nr), s) in
     stop_at_location e_loc r 19
  | _ -> None



and sscondpat env e_cond s = sexp env e_cond s

and sstate genv env { desc; loc } s =
  match desc, s with
  | Estate0(f), Sempty -> return (Value(Vstate0(f)), Sempty)
  | Estate1(f, e_list), Stuple s_list ->
     let* v_env_s_list = Opt.map2 (sexp env) e_list s_list in
     let v_s_list = List.map (fun (a,b,c) -> (a, c)) v_env_s_list in
     let v_list, s_list = List.split v_s_list in
     return (Value(Vstate1(f, v_list)), Stuple(s_list))
  | _ -> None

and smatch_handler_list genv env ve eq_write m_h_list s_list =
  match m_h_list, s_list with
  | [], [] -> None
  | { m_pat; m_vars; m_body } :: m_h_list,
    (Stuple [Stuple(ss_var_list); ss_body] as s) :: s_list ->
     let r = matching_pattern ve m_pat in
     let* env_handler, s_list =
       match r with
       | None ->
          (* this is not the good handler; try an other one *)
          let* env_handler, s_list =
            smatch_handler_list genv env ve eq_write m_h_list s_list in
          return (env_handler, s :: s_list)
       | Some(env_pat) ->
          let env = Env.append env_pat env in
          let* _, env_handler, ss_var_list, ss_body =
            sblock env m_vars m_body ss_var_list ss_body in
          return
            (env_handler, Stuple [Stuple(ss_var_list); ss_body] :: s_list) in
     (* complete missing entries in the environment *)
     (*let* env_handler = by env env_handler eq_write in*)
     return (env_handler, s_list)
  | _ -> None

in
let env, s =
(* Init 0a *)
if Option.is_some init_state1
then
  let f_args, s_f_args, v_list = Option.get init_state1 in
  let opt =
    Opt.mapfold3 (svardec Env.empty)
      Env.empty f_args s_f_args v_list in
  let env_args, s_f_args =
    if Option.is_some opt
    then Option.get opt
    else failwith "Init 0a failed." in
  env_args, Stuple s_f_args
else
(* Init 0b *)
if Option.is_some init_state2
then
  let env_args, f_res, s_f_res = Option.get init_state2 in
  let opt =
    Opt.mapfold3 (svardec env_args)
      Env.empty f_res s_f_res (bot_list f_res) in
  let env_res, s_f_res =
    if Option.is_some opt
    then Option.get opt
    else failwith "Init 0b failed." in
  env_res, Stuple s_f_res
else
(* Init 0c *)
(*if Option.is_some init_state3

else*)
(* stateful/sequential case *)
if smap <> Env2.empty
then begin
  let s = Env2.find main_eq smap in
  (*let opt = seq init_env main_eq s in*)
(*Format.eprintf "\n############ VAL %i\n" (Env.cardinal init_env2);*)
  let opt = seq init_env2 main_eq s in
  let env, s =
    if Option.is_some opt
    then Option.get opt
    else failwith "Sequential case : eval failed." in
  env, s
(* combinatorial case *)
end else
  let s_opt = ieq genv Env.empty main_eq in
  if Option.is_some s_opt
  then
    let s = Option.get s_opt in
    (*let opt = seq init_env main_eq s in*)
    (* Env.empty -> init_env2 *)
    let opt = seq init_env2 main_eq s in
    let env, _ =
      if Option.is_some opt
      then Option.get opt
      else failwith "Combinatorial case : eval failed." in
    env, Sempty
  else failwith "none3..."
in
(env, main_eq.eq_write, s))
end


let matching_in env { var_name; var_default } v =
  return (Env.add var_name { cur = v; default = Val } env)

let matching_out env { var_name; var_default } =
  find_value_opt var_name env

let vardec_to_ident { var_name } = var_name

let funexp genv { f_kind; f_atomic; f_args; f_res; f_body; f_loc } =
  let* si = ieq genv Env.empty f_body in
  let f = match f_kind with
  | Efun ->
     (* combinatorial function *)
     return
       (CoFun
          (fun v_list ->
            let* env =
              Opt.fold2 matching_in Env.empty f_args v_list in
            let cowritem = Cowrite3.cowrite f_body in
            (*let init_env2 = sdeq*)
            (* dans l'implémentation originale, on donne l'état ici *)
            (* TODO : à bouger pour init_env2 *)
            (*let* env, _ = seq genv env f_body si in*)
            (* ici, il est donné à l'intérieur de globeval *)
            let valuation = Make.globeval cowritem env Env.empty genv f_body None None None in
            let env, dom, s = valuation f_body in
            (*List.iter (fun x -> Format.eprintf "\n!!!!!%s : " (string_of x);
                    Pprinter.pp_value_ext (Env.find x env).cur) (List.map vardec_to_ident f_res);*)
            let* v_list = Opt.map (matching_out env) f_res in
            return (v_list)))
  | Enode ->
     (* stateful function *)
     (* add the initial state for the input and output vardec *)
     let* s_f_args = Opt.map (ivardec genv Env.empty) f_args in
     let* s_f_res = Opt.map (ivardec genv Env.empty) f_res in
     return
       (CoNode
          { init = Stuple [Stuple(s_f_args); Stuple(s_f_res); si];
            step =
              fun s v_list ->
              match s with
              | Stuple [Stuple(s_f_args); Stuple(s_f_res); s_body] ->
                 (* associate the input value to the argument *)
                 (* let* env_args, s_f_args =
                   Opt.mapfold3 (svardec genv Env.empty)
                     Env.empty f_args s_f_args v_list in *)
                 (* un peu hackish, pour éviter la duplication de code *)
                 let valuation0a = Make.globeval Env.empty Env.empty Env.empty genv f_body
                   None (Some (f_args, s_f_args, v_list)) None in
                 let dummy_eq0a = { eq_desc = EQempty;
                                    eq_write = S.empty;
                                    eq_loc = Loc (-1, -1) } in
                 let env_args, _, s_f_args = valuation0a dummy_eq0a in
                 let Stuple s_f_args = s_f_args in

                 (* let* env_res, s_f_res =
                   Opt.mapfold3 (svardec genv env_args)
                     Env.empty f_res s_f_res (bot_list f_res) in *)
                 (* un peu hackish, pour éviter la duplication de code *)
                 let valuation0b = Make.globeval Env.empty env_args Env.empty genv f_body
                   None None (Some (env_args, f_res, s_f_res)) in
                 let dummy_eq0b = { eq_desc = EQempty;
                                    eq_write = S.empty;
                                    eq_loc = Loc (-2, -2) } in
                 let env_res, _, s_f_res = valuation0b dummy_eq0b in
                 let Stuple s_f_res = s_f_res in

                 print_ienv "Node before fixpoint (args)" env_args;
                 print_ienv "Node before fixpoint (res)" env_res;

                 let env_args_res = Env.union (fun k v1 v2 -> Some v2) env_args env_res in

                 let cowritem = Cowrite3.cowrite f_body in
                 let smap = Cowrite3.associate_eq Env2.empty f_body s_body in
                 let init_env2 = sdeq env_args_res f_body s_body in
                 let init_env2 = Option.get init_env2 in

                 (*let valuation = Make.globeval cowritem env_args genv f_body*)
                 let valuation = Make.globeval cowritem env_args init_env2 genv f_body
                   (Some smap) None None in
                 let env_body, _, s_body = valuation f_body in
                 (*
                 let* env_body, s_body =
                   if !only_one_fixpoint = 1
                   then seq genv env_args f_body s_body
                   else fixpoint_eq genv env_args seq f_body n s_body env_res
                 in
                 print_ienv "Node after fixpoint (body)" env_body;
                 *)
                 (*List.iter (fun x -> Format.eprintf "\n!!!!!%s : " (string_of x);
                    Pprinter.pp_value_ext (Env.find x env_body).cur) (List.map vardec_to_ident f_res);*)

                 let* s_f_res = Opt.map2 (set_vardec env_body) f_res s_f_res in
                 let* v_list = Opt.map (matching_out env_body) f_res in
                 return (v_list,
                         (Stuple [Stuple(s_f_args); Stuple(s_f_res); s_body]))
              | _ -> None })
  in
  stop_at_location f_loc f 10

(*
let exp genv env e =
  let* init = iexp genv env e in
  let step s = sexp genv env e s in
  return (CoF { init = init; step = step })
*)
  
let implementation genv { desc; loc } =
  let r = match desc with
  | Eletdecl(f, e) -> failwith "not implemented"
     (*(* [e] should be stateless, that is, [step s = v, s] *)
     let* si = iexp genv Env.empty e in
     let* v, s = sexp genv Env.empty e si in
     return (Genv.add (Name(f)) (Gvalue(v)) genv)*)
  | Eletfundecl(f, fd) ->
     let* fv = funexp genv fd in
     return (Genv.add (Name(f)) (Gfun(fv)) genv)
  | Etypedecl(f, td) ->
     return genv
  in
  stop_at_location loc r 11
     
let program genv i_list = Opt.fold implementation genv i_list

(* check that a value is causally correct (non bot) *)
(* and initialized (non nil) *)
let not_bot_nil v_list =
  let not_bot_nil v =
    match v with
    | Vbot ->
       Format.eprintf "Causality error [ARF].@."; raise Stdlib.Exit
    | Vnil ->
       Format.eprintf "Initialization error.@."; raise Stdlib.Exit
    | Value(v) ->
       return v in
  Opt.map not_bot_nil v_list
    
(* run a combinatorial expression *)
(* returns the number of successful iterations *)
let run_fun output fv n =
  let rec runrec i =
    if i = n then i
    else
      let v =
        let* v_list = fv [] in
        let* v_list = not_bot_nil v_list in
        output v_list in
      if Option.is_none v then i
      else runrec (i+1) in
  runrec 0
      
(* run a stream process *)
let run_node output init step n =
  let rec runrec s i =
    if i = n then i
    else
      let v =
        let* v_list, s = step s [] in
        let* v_list = not_bot_nil v_list in
        let* _ = output v_list in
        return s in
      match v with | None -> i | Some(s) -> runrec s (i+1) in
  runrec init 0

(* The main entry function *)
let run genv main ff n =
  let* fv = find_gnode_opt (Name main) genv in
  (* the main function must be of type : unit -> t *)
  let output v_list =
    let _ = Output.pvalue_list_and_flush ff v_list in
    return () in
  match fv with
  | CoFun(fv) ->
     let _ = run_fun output fv n in
     return ()
  | CoNode { init; step } ->
     let _ = run_node output init step n in
     return ()

let check genv main n =
  let* fv = find_gnode_opt (Name main) genv in
  (* the main function must be of type : unit -> bool *)
  let output v =
    if v = [Vbool(true)] then return () else None in
  match fv with
  | CoFun(fv) ->
     let i = run_fun output fv n in
     if i < n then Format.printf "Test failed: only %i iterations.\n" i;
     return ()
  | CoNode { init; step } ->
     let i = run_node output init step n in
     if i < n then Format.printf "Test failed: only %i iterations.\n" i;
     return ()
 

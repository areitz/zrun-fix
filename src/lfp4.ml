open Fix

open Ident
open Value
open Common
open Debug

module Make = struct
  module V = struct
    type t = Ident.t
  end
  module F =
    Fix.ForType(V)(P3)

  let s = ref true

  let globeval dom sem s_eq env =
    F.lfp
      (fun id valuation ->
        let env2 = if !s
        then begin
        s := false;
        List.fold_left
          (fun env id ->
             try
               let v = valuation id in
               Env.add id v env
             with
             | Not_found -> env
          )
          env dom
        end
        else env
        in
        print_ienv "#1: " env;
        print_ienv "#2: " env2;
        let s = sem s_eq env2 in
        if Option.is_some s
        then
          begin
          let r = Env.find id (fst (Option.get s)) in
          r
          end
        else
          Env.find id env2
      )
end


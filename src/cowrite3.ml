open Ast
open Misc
open Value
open Ident
open Ident2
open Location

(* in order to respect the usual fixpoint computation hierarchy and thus semantics *)
let is_local { eq_desc } = match eq_desc with
  | EQlocal _ -> true
  | _ -> false

let i = ref 0
let rec write_eq ({ eq_desc } as eq) =
  incr i;
  let loc = Loc (!i, !i) in
  match eq_desc with
  | EQempty ->
      { eq_desc = EQempty; eq_write = S.empty; eq_loc = loc }
  | EQeq (p, e) ->
      let e, dom = write_exp e in
      let dom = List.fold_left
        (fun acc id -> S.add id acc)
        dom p.desc in
      { eq_desc = EQeq (p, e); eq_write = dom; eq_loc = loc }
  | EQand eql ->
      let eql = List.map write_eq eql in
      let dom = List.fold_left
        (fun acc eq -> S.union eq.eq_write acc)
        S.empty eql in
      { eq_desc = EQand eql; eq_write = dom; eq_loc = loc }
  | EQlocal (vl, e) ->
      (* on pourrait vérifier que vl \subset dom *)
      (* TODO vl *)
      let e = write_eq e in
      let dom = e.eq_write in
      { eq_desc = EQlocal(vl, e); eq_write = dom; eq_loc = loc }
  | EQif (e, eq1, eq2) ->
      let e, dom = write_exp e in
      let eq1 = write_eq eq1 in
      let eq2 = write_eq eq2 in
      let dom = S.union dom (S.union eq1.eq_write eq2.eq_write) in
      { eq_desc = EQif(e, eq1, eq2); eq_write = dom; eq_loc = loc }
  | EQreset (eq, e) ->
      let eq = write_eq eq in
      let e, dom = write_exp e in
      let dom = S.union dom eq.eq_write in
      { eq_desc = EQreset(eq, e); eq_write = dom; eq_loc = loc }
  | EQassert (e) ->
      let e, dom = write_exp e in
      { eq_desc = EQassert(e); eq_write = dom; eq_loc = loc }
   | EQmatch (e, mh_list) ->
      let e, dom = write_exp e in
      let mh_list, dom = List.fold_left
        (fun (acc_mhl, acc_dom) mh ->
          let mh, dom = match_handler acc_dom mh in (mh::acc_mhl, dom))
        ([], dom) mh_list in
      let mh_list = List.rev mh_list in
      { eq_desc = EQmatch (e, mh_list); eq_write = dom; eq_loc = loc }
   | EQautomaton (b, ah_list) ->
      (* TODO : missing escapes *)
      let ah_list, dom = List.fold_left
        (fun (acc_ahl, acc_dom) ah ->
          let ah, dom = automaton_handler acc_dom ah in (ah::acc_ahl, dom))
        ([], S.empty) ah_list in
      let ah_list = List.rev ah_list in
      { eq_desc = EQautomaton(b, ah_list); eq_write = dom; eq_loc = loc }

and write_exp ({ e_desc } as exp) = match e_desc with
  | Econst _ -> exp, S.empty
  | Elocal _ | Elast _ -> exp, S.empty
  | Econstr0 _ -> exp, S.empty
  | Etuple expl ->
      let expl, doml = List.split (List.map write_exp expl) in
      let dom = List.fold_left
        (fun acc dom -> S.union dom acc)
        S.empty doml in
      { exp with e_desc = Etuple expl }, dom
  | Eget (i, e) ->
      let e, dom = write_exp e in
      { exp with e_desc = Eget (i, e) }, dom
  | Eop (op, expl) ->
      let expl, doml = List.split (List.map write_exp expl) in
      let dom = List.fold_left
        (fun acc dom -> S.union dom acc)
        S.empty doml in
      { exp with e_desc = Eop (op, expl) }, dom
  | Eapp (lid, expl) ->
      let expl, doml = List.split (List.map write_exp expl) in
      let dom = List.fold_left
        (fun acc dom -> S.union dom acc)
        S.empty doml in
      { exp with e_desc = Eapp (lid, expl) }, dom
  | Elet (b, eq, e) ->
      let eq = write_eq eq in
      let e, dom = write_exp e in
      let dom = S.union eq.eq_write dom in
      { exp with e_desc = Elet (b, eq, e) }, dom

and match_handler acc ({ m_vars; m_body } as m) =
  let m_vars = m_vars in
  let m_body = write_eq m_body in
  let dom = m_body.eq_write in
  let dom = S.union acc dom in
  { m with m_body = m_body }, dom

and automaton_handler acc ({ s_vars; s_body } as a) =
  (* TODO : missing escapes *)
  let s_vars = s_vars in
  let s_body = write_eq s_body in
  let dom = s_body.eq_write in
  let dom = S.union acc dom in
  { a with s_body = s_body }, dom

let write_impl ({ desc } as impl) = match desc with
  | Eletdecl _ -> failwith "not implemented"
  | Eletfundecl (name, funexp) ->
      let w_body = write_eq funexp.f_body in
      let funexp = { funexp with f_body = w_body } in
      { impl with desc = Eletfundecl (name, funexp) }
  | Etypedecl _ -> impl

let write_program = List.map write_impl

(* 0 -> 1 voire 3 ? *)
(* besoin de deux variables : sous un if = pê mal défini de manière indétectale -> eqlocal *)
let cfg_level = 1
let cfg_switch main_eq cfg_opt = match cfg_level, main_eq.eq_desc with
| 0, (EQif _ | EQreset _ | EQmatch _) -> Some main_eq
| 1, (EQif _ | EQreset _ | EQmatch _) -> if Option.is_some cfg_opt
             then cfg_opt
             else Some main_eq
| 2, (EQlocal _ | EQif _ | EQreset _) -> Some main_eq
| 3, (EQlocal _ | EQif _ | EQreset _) -> if Option.is_some cfg_opt
             then cfg_opt
             else Some main_eq
| _, _ -> None


let rec cowrite_eq_aux m cfg_opt ({ eq_desc } as main_eq) =
  match eq_desc with
  | EQempty -> m
  | EQlocal (_, eq) ->
     let cfg_opt = cfg_switch main_eq cfg_opt in
     cowrite_eq_aux m cfg_opt eq
  | EQeq (p, e) ->
     let m = cowrite_exp m cfg_opt e in
     List.fold_left
       (fun m k ->
         let v = if Env.mem k m
                 then Env.find k m
                 else [] in
         if Option.is_none cfg_opt
         then Env.add k (main_eq::v) m
         else Env.add k ((Option.get cfg_opt)::v) m
       )
       m p.desc
  | EQand eql ->
     List.fold_left
       (fun m eq -> cowrite_eq_aux m cfg_opt eq)
       m eql
  | EQif (e, eq1, eq2) ->
     let m = cowrite_exp m cfg_opt e in
     let cfg_opt = cfg_switch main_eq cfg_opt in
     let m = cowrite_eq_aux m cfg_opt eq1 in
     let m = cowrite_eq_aux m cfg_opt eq2 in
     m
  | EQreset (eq, e) ->
     let cfg_opt = cfg_switch main_eq cfg_opt in
     let m = cowrite_eq_aux m cfg_opt eq in
     let m = cowrite_exp m cfg_opt e in
     m
  | EQassert (e) ->
     cowrite_exp m cfg_opt e
  | EQmatch (e, mhl) ->
     let m = cowrite_exp m cfg_opt e in
     let cfg_opt = cfg_switch main_eq cfg_opt in
     List.fold_left
       (fun m mh -> cowrite_eq_aux m cfg_opt mh.m_body)
       m mhl
  | EQautomaton (_, ahl) ->
     (* TODO : missing escapes *)
     let cfg_opt = cfg_switch main_eq cfg_opt in
     List.fold_left
       (fun m ah -> cowrite_eq_aux m cfg_opt ah.s_body)
       m ahl

and cowrite_exp m cfg_opt ({ e_desc } as main_exp) =
  match e_desc with
  | Econst _ | Elocal _ | Elast _ | Econstr0 _ -> m
  | Eop (_, el) | Etuple el | Eapp (_, el) ->
     List.fold_left
       (fun m e -> cowrite_exp m cfg_opt e)
       m el
  | Eget (_, e) -> cowrite_exp m cfg_opt e
  | Elet (_, eq, e) ->
     let old_cfg_opt = cfg_opt in
     let cfg_opt = None in
     let m = cowrite_eq_aux m cfg_opt eq in
     let cfg_opt = old_cfg_opt in
     let m = cowrite_exp m cfg_opt e in
     m

let cowrite eq = cowrite_eq_aux Env.empty None eq

let rec associate_eq m eq s =
  let m = Env2.add eq s m in
  match eq.eq_desc, s with
  | EQempty, s -> m
  | EQeq (_, e), s -> associate_exp m e s
  (*| EQlocal (_, eq'), s ->
      let m = Env2.add eq s m in
      associate_eq m eq' s*)
  | EQlocal (_, eq'), Stuple [Stuple _; s_eq] -> associate_eq m eq' s_eq
  | EQand eql, Stuple s_list ->
      List.fold_left2
        (fun m eq s -> associate_eq m eq s)
        m eql s_list
  | EQif (eb, eq1, eq2), Stuple [seb; s_eq1; s_eq2] ->
      let m = associate_exp m eb seb in
      let m = associate_eq m eq1 s_eq1 in
      let m = associate_eq m eq2 s_eq2 in
      m
  | EQassert e, s -> m
  | EQreset (eq', e), Stuple [s_eq; se] ->
      let m = associate_eq m eq' s_eq in
      let m = associate_exp m e se in
      m
  | EQmatch (e, mhl), Stuple (se :: smhl) ->
      let m = associate_exp m e se in
      List.fold_left2
        (fun m mh (Stuple [_; s]) -> associate_eq m mh.m_body s)
        m mhl smhl
  | EQautomaton (_, ahl), Stuple (Sval ps :: Sval pr :: s_list) ->
      (* TODO : missing escapes *)
      List.fold_left2
        (fun m ah (Stuple [_; s; _]) -> associate_eq m ah.s_body s)
        m ahl s_list

and associate_exp m exp s = match exp.e_desc, s with
  | Econst _, _ | Elocal _, _ | Elast _, _ | Econstr0 _, _ -> m
  | Eget (_, e), s -> associate_exp m e s
  | Etuple el, Stuple sl ->
      List.fold_left2
        (fun m exp s -> associate_exp m exp s)
        m el sl
  | Eapp (f, el), Stuple (s :: sl) ->
      List.fold_left2
        (fun m exp s -> associate_exp m exp s)
        m el sl
  | Elet (_, eq, e), Stuple [s_eq; s] ->
      let m = associate_eq m eq s_eq in
      let m = associate_exp m e s in
      m
  | Eop (Eifthenelse, [e1; e2; e3]), Stuple [s1; s2; s3] ->
      let m = associate_exp m e1 s1 in
      let m = associate_exp m e2 s2 in
      let m = associate_exp m e3 s3 in
      m
  | Eop(Eminusgreater, [e1; e2]), Stuple [Sval v; s1; s2] ->
      let m = associate_exp m e1 s1 in
      let m = associate_exp m e2 s2 in
      m
  | Eop(Eunarypre, [e]), Stuple [Sval v; s] ->
      let m = associate_exp m e s in
      m
  | Eop(Efby, [_; e2]), Stuple [Sval v; s2] ->
      let m = associate_exp m e2 s2 in
      m
  | Eop(Efby, [e1; e2]), Stuple [Sopt v_opt; s1; s2] ->
      let m = associate_exp m e1 s1 in
      let m = associate_exp m e2 s2 in
      m
  | _, Slazy _ -> m 
  | _, _ -> Format.eprintf "||";
Pprinter.pp_exp exp; Pprinter.pp_state s; m

let printmeqs m =
  Env2.iter
    (fun k v -> Pprinter.pp_equation k;
                Format.eprintf " : ";
                Pprinter.pp_state v;
                Format.eprintf "\n") m

import sys

def content(n):
    L = []
    def i_to_x(i):
        xi1 = "x"+str(i)
        xi2 = "x"+str(i)+"'"
        return xi1, xi2
    for i in range(n):
        xi1, xi2 = i_to_x(i)
        L.append("local "+xi1+","+xi2+" do")
    for i in range(n):
        xi1, xi2 = i_to_x(i)
        xj1, xj2 = i_to_x(i+1)
        L.append(xi1+" = "+xi2+" + 1")
        L.append("and")
        if i == n-1:
           L.append(xi2+" = x + 1")
        else:
           L.append(xi2+" = "+xj1+" + 1")
        L.append("and")
    L.append("x = 0")
    L.append("and")
    L.append("y = x0")
    for i in range(n):
        L.append("done")
    return L

def nodify(L):
    acc = []
    n = len(L)
    acc.append("let node main() returns (x, y)")
    acc += L
    return acc

def write(L, name):
    with open("gentests/"+name+".zls", "w") as f:
        for v in L:
            f.write(v+"\n")
        f.close()

def main():
    n = int(sys.argv[1])
    L = content(n)
    N = nodify(L)
    write(N, "nst"+str(n))
main()
